<!DOCTYPE HTML>
<html>
<head>
    <title>Vloz novy udaj</title>
    <meta charset='utf-8'>

    <style type="text/css">
        label { width: 10em;
            float: left; }
        fieldset { width: 500px; }
        legend { margin: 0 0 0 70%;
            width: 120px;
            text-align:center; }
        select {width: 100px;}
    </style>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="style.css" charset=utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet">
</head>
<body>
<div class="container">
    <form action="" class="form-horizontal" method="get">
        <div class="form-group">
            <h2>Vloz novy udaj</h2>
            <p><label for="meno">Meno</label>
                <input type="text" id="meno" name="meno" size="40"></p>
            <p><label for="priezvisko">Priezvisko</label>
                <input type="text" id="priezvisko" name="priezvisko" size="40"></p>
            <p><label for="datum">Datum narodenia</label>
                <input type="date" id="datum" name="datum" size="40"></p>
            <p><label for="narodenie">Miesto narodenia</label>
                <input type="narodenie" id="narodenie" name="narodenie" size="40"></p>
            <p><label for="stat">Štát</label>
                <input type="text" id="stat" name="stat" size="40"></p>
            <p><label for="datumsmrt">Datum smrti</label>
                <input type="date" id="datumsmrt" name="datumsmrt" size="40"></p>
            <p><label for="smrtmiesto">Miesto smrti</label>
                <input type="text" id="smrtmiesto" name="smrtmiesto" size="40"></p>
            <p><label for="smrtstat">Štát kde zomrel</label>
                <input type="text" id="smrtstat" name="smrtstat" size="40"></p>

            <p><label for="typ">Typ</label>
                <input type="text" id="typ" name="typ" size="40"></p>
            <p><label for="rok">Rok konania</label>
                <input type="number" id="rok" name="rok" size="40"></p>
            <p><label for="umiestnenie">Umiestnenie</label>
                <input type="number" id="umiestnenie" name="umiestnenie" size="40"></p>
            <p><label for="mesto">Mesto konania</label>
                <input type="text" id="mesto" name="mesto" size="40"></p>
            <p><label for="krajina">Krajina konania</label>
                <input type="text" id="krajina" name="krajina" size="40"></p>

            <p><label for="discip">Disciplína</label>
                <input type="text" id="discip" name="discip" size="40"></p>


        </div>

        <input type="submit" name="submit" class="btn btn-success" value="Odošli">
    </form>
</div>


<?php



if (isset($_GET['submit'])) {
    require "config.php";
//nacitat config

// Create connection
    $conn = new mysqli($serverName, $userName, $password, $dbname);
    $conn->set_charset("utf8");
// Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }


    $meno = $_GET["meno"];
    $priezvisko = $_GET["priezvisko"];
    $datum = $_GET["datum"];
    $narodenie = $_GET["narodenie"];
    $stat = $_GET["stat"];
    $datumsmrt = $_GET["datumsmrt"];
    $smrtmiesto = $_GET["smrtmiesto"];
    $smrtstat = $_GET["smrtstat"];

    $typ = $_GET["typ"];
    $rok = $_GET["rok"];
    $umiestnenie = $_GET["umiestnenie"];
    $mesto = $_GET["mesto"];
    $krajina = $_GET["krajina"];
    $discip = $_GET["discip"];

    if($meno != NULL && $priezvisko != NULL ){
       $sql = "INSERT INTO `persons` (`id`, `name`, `surname`, `birthDay`, `birthPlace`, `birthCountry`, `deathDay`, `deathPlace`, `deathCountry`)
     VALUES (NULL, '$meno', '$priezvisko', '$datum', '$narodenie', '$stat', '$datumsmrt', '$smrtmiesto', '$smrtstat')";
        if ($conn->query($sql) === TRUE) {
           // echo "New record created successfully";
        } else {
           // echo "Error: " . $sql . "<br>" . $conn->error;
        }
        $person_id = $conn->insert_id;
    }
    if( $typ != NULL && $rok != NULL && $umiestnenie != NULL && $mesto != NULL &&  $krajina != NULL ){

        $sql = "INSERT INTO `oh` (`id`, `type`, `year`, `placement`, `city`, `country`) 
VALUES (NULL, '$typ', '$rok', '$umiestnenie', '$mesto', '$krajina')";
        if ($conn->query($sql) === TRUE) {
           // echo "New record created successfully";
        } else {
           // echo "Error: " . $sql . "<br>" . $conn->error;
        }
        $oh_id = $conn->insert_id;
    }

    if($discip != NULL && $umiestnenie != NULL && $mesto != NULL && $meno != NULL && $priezvisko != NULL ){


//        $sql = "SELECT persons.id as id_persons FROM `persons` WHERE persons.name = $meno AND persons.surname = $priezvisko";
//
//        $sql = "SELECT oh.id AS ID_OH FROM `oh` WHERE oh.placement = $umiestnenie AND oh.city = $mesto";
//
//        $sql = "SELECT * FROM `persons` WHERE persons.id = `id_person`";
//        $sql = "SELECT * FROM `oh` WHERE oh.country = $krajina";




//        $sql1 = "SELECT persons.id as id_persons FROM `persons` WHERE persons.name = $meno AND persons.surname = $priezvisko";
//        $result1 = $conn->query($sql1);
//
//
//        $sql2 = "SELECT oh.id AS ID_OH FROM `oh` WHERE oh.placement = '$umiestnenie' AND oh.city = $mesto";
//        $result2 = $conn->query($sql2);
//
//
//
//        if ($result1->num_rows > 0 && $result2->num_rows2 > 0 ) {
//            // output data of each row
//            while($row1 = $result->fetch_assoc() && $row2 = $result2->fetch_assoc()) {
                $sql = "INSERT INTO `umiestnenie` 
                  (`id`, `id_person`, `ID_OH`, `place`, `discipline`) 
              VALUES (NULL,  '$person_id', '$oh_id', '$umiestnenie', '$discip')";
//            }
//        }




        if ($conn->query($sql) === TRUE) {
           // echo "New record created successfully";
        } else {
          //  echo "Error: " . $sql . "<br>" . $conn->error;
        }

    }








    $conn->close();
    echo "<a class=\"btn container\" href='index.php'>Choď späť hlavnu stranku</a>";
}


//$sql = "DELETE FROM (SELECT umiestnenie.id ,id_person AS idPe , ID_OH, persons.name, persons.surname, oh.year, oh.city, oh.country, oh.type, discipline, place FROM umiestnenie LEFT JOIN oh ON umiestnenie.ID_OH= oh.id LEFT JOIN persons ON umiestnenie.id_person= persons.id)
//        AS sporotvci WHERE  sportovci.id = ". $inde ." AND sportovci.idPe = ". $indePer ;



?>
</body>
</html>
