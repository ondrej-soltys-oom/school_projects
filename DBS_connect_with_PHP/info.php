<html lang="sk">
<head>
    <title>Športovci</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="style.css">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet">
    <style></style>
</head>
<body>
<div class="container">
    <br><br>
    <h1 class="text-center">Informácie o športovcovi</h1><br><br><br><br>
    <?php
    require "config.php";
    //nacitat config
    $inde = $_GET["index"];
    $indePer = $_GET["idPer"];

    // Create connection
    $conn = new mysqli($serverName, $userName, $password, $dbname);
    $conn->set_charset("utf8");
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    $sql = "SELECT umiestnenie.id ,id_person, ID_OH, persons.name, persons.surname, persons.birthDay, persons.birthPlace, oh.year, oh.city, oh.country, oh.type, discipline, place FROM umiestnenie LEFT JOIN oh ON umiestnenie.ID_OH= oh.id LEFT JOIN persons ON umiestnenie.id_person= persons.id WHERE persons.id = '$indePer' LIMIT 1";
    //    $sql = "SELECT umiestnenie.id ,id_person, ID_OH, persons.name, persons.surname, oh.year, oh.city, oh.country, oh.type, discipline, place FROM umiestnenie LEFT JOIN oh ON umiestnenie.ID_OH= oh.id LEFT JOIN persons ON umiestnenie.id_person= persons.id";
    $result = $conn->query($sql);
    if ($result->num_rows>0){
        while ($row = $result->fetch_assoc()) {



            echo "<div \"container\">" .
                "<p>".  "Meno" .  $row["name"] . "</p>" .
                "<p>" . "Priezvisko" . $row["surname"] . "</p>" .
                "<p>" . "Dátum narodenia" . $row["birthDay"] . "</p>" .
                "<p>" . "Mesto" . $row["birthPlace"] . "</p>" .
                "</div>";



        }
    }
    //    $sql = "SELECT umiestnenie.id ,id_person, ID_OH, persons.name, persons.surname, oh.year, oh.city, oh.country, oh.type, discipline, place FROM umiestnenie LEFT JOIN oh ON umiestnenie.ID_OH= oh.id LEFT JOIN persons ON umiestnenie.id_person= persons.id";

    ?>
    <div class="table-responsive">
        <table class="table table-striped table-hover">
            <thead class="thead-dark">
            <?php
            require "config.php";
            //nacitat config
            $inde = $_GET["index"];
            $indePer = $_GET["idPer"];

            // Create connection
            $conn = new mysqli($serverName, $userName, $password, $dbname);
            $conn->set_charset("utf8");
            // Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }
            $sql = "SELECT umiestnenie.id ,id_person, ID_OH, persons.name, persons.surname, oh.year, oh.city, oh.country, oh.type, discipline, place FROM umiestnenie LEFT JOIN oh ON umiestnenie.ID_OH= oh.id LEFT JOIN persons ON umiestnenie.id_person= persons.id WHERE persons.id = '$indePer'";
            //    $sql = "SELECT umiestnenie.id ,id_person, ID_OH, persons.name, persons.surname, oh.year, oh.city, oh.country, oh.type, discipline, place FROM umiestnenie LEFT JOIN oh ON umiestnenie.ID_OH= oh.id LEFT JOIN persons ON umiestnenie.id_person= persons.id";
            $result = $conn->query($sql);
            if ($result->num_rows>0){
                while ($row = $result->fetch_assoc()) {

                    echo "<tr>" .
                        "<td>" . $row["year"] . "</td>" .
                        "<td>" . $row["city"] . "</td>" .
                        "<td>" . $row["country"] . "</td>" .
                        "<td>" . $row["type"] . "</td>" .
                        "<td>" . $row["discipline"] . "</td>" .
                        "<td>" . $row["place"] . "</td>" .
                        "</tr>" ;



                }
            }
            ?>
            </tbody>
        </table>
        <div class="text-center"><a href="index.php" class="btn btn-info text-center" role="button">Návrat na hlavnú
                stránku</a></div>
    </div>
</div>
</body>
</html>