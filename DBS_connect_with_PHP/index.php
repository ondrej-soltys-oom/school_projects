<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Športovci</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="style.css" charset=utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet">
</head>
<body>


<div class="container">
    <p class="h1 text-center">Športovci na OH</p>
    <br>
    <div class="table-responsive">
        <table class="table table-striped table-hover">
            <thead class="thead-dark">
            <tr>
                <th><a id="headlink" href="index.php?zotried=1">Meno</a></th>
                <th><a id="headlink" href="index.php?zotried=2">Priezvisko</a></th>
                <th><a id="headlink" href="index.php?zotried=3">Rok</a></th>
                <th>Miesto</th>
                <th>Krajina</th>
                <th><a id="headlink" href="index.php?zotried=4">Typ</a></th>
                <th>Disciplína</th>
                <th>Umiestnenie</th>
                <th></th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <?php

            require "config.php";
            //nacitat config


            // Create connection
            $conn = new mysqli($serverName, $userName, $password, $dbname);
            $conn->set_charset("utf8");
            // Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }

            if(isset($_GET["zotried"])){
                $order = $_GET["zotried"];
            }
            else{
                $order = 0;
            }




            switch ($order){
                case 1:
                    //podla mena
                    $sql = "SELECT umiestnenie.id ,id_person, ID_OH, persons.name, persons.surname, oh.year, oh.city, oh.country, oh.type, discipline, place FROM umiestnenie LEFT JOIN oh ON umiestnenie.ID_OH= oh.id LEFT JOIN persons ON umiestnenie.id_person= persons.id ORDER BY persons.name ASC";

                    break;
                case 2:
                    //podla priezviska
                    $sql = "SELECT umiestnenie.id ,id_person, ID_OH, persons.name, persons.surname, oh.year, oh.city, oh.country, oh.type, discipline, place FROM umiestnenie LEFT JOIN oh ON umiestnenie.ID_OH= oh.id LEFT JOIN persons ON umiestnenie.id_person= persons.id ORDER BY persons.surname ASC";

                    break;
                case 3:
                    //podla roku
                    $sql = "SELECT umiestnenie.id ,id_person, ID_OH, persons.name, persons.surname, oh.year, oh.city, oh.country, oh.type, discipline, place FROM umiestnenie LEFT JOIN oh ON umiestnenie.ID_OH= oh.id LEFT JOIN persons ON umiestnenie.id_person= persons.id ORDER BY oh.year ASC";

                    break;
                case 4:
                    //podla typ
                    $sql = "SELECT umiestnenie.id ,id_person, ID_OH, persons.name, persons.surname, oh.year, oh.city, oh.country, oh.type, discipline, place FROM umiestnenie LEFT JOIN oh ON umiestnenie.ID_OH= oh.id LEFT JOIN persons ON umiestnenie.id_person= persons.id ORDER BY `oh`.`type`  ASC";
                    break;
                default:
                    $sql = "SELECT umiestnenie.id ,id_person, ID_OH, persons.name, persons.surname, oh.year, oh.city, oh.country, oh.type, discipline, place FROM umiestnenie LEFT JOIN oh ON umiestnenie.ID_OH= oh.id LEFT JOIN persons ON umiestnenie.id_person= persons.id";
                    break;

            }
            $result = $conn->query($sql);
            if ($result->num_rows>0){
                while ($row = $result->fetch_assoc()) {


                    echo "<tr>" .
                        "<td>" . "<a href=\"info.php?index=" . $row["ID_OH"] . "&amp;idPer= " . $row["id_person"] . " \">" . $row["name"] . "</a>". "</td>" .
                        "<td>" . $row["surname"] . "</td>" .
                        "<td>" . $row["year"] . "</td>" .
                        "<td>" . $row["city"] . "</td>" .
                        "<td>" . $row["country"] . "</td>" .
                        "<td>" . $row["type"] . "</td>" .
                        "<td>" . $row["discipline"] . "</td>" .
                        "<td>" . $row["place"] . "</td>" .
                        "<td>" . "<a href=\"edit.php?index=" . $row["ID_OH"] . "&amp;idPer= " . $row["id_person"] . " \"><i class=\"fas fa-pencil-alt\"></i></a>" . "</td>" .
                        "<td>" . "<a href=\"delete.php?index=" . $row["id"] . "&amp;idPer= " . $row["id_person"] . " \"><i class=\"fas fa-ban\"></i></a>" . "</td>" .
                        "</tr>" ;
//        echo "<tr>" . "-" . $row["surname"] . "-" . $row["year"]
//            . "-" . $row["city"] . "-" . $row["country"] . "-" . $row["type"] . "-"
//            . $row["discipline"] . "-" . $row["place"] . "<br>";
                }
            }


            ?>
            <!--<tr>-->
                <!--<td><a href="info.php?inf=1">Peter</a></td>-->
                <!--<td>Hochschorner</td>-->
                <!--<td>2000</td>-->
                <!--<td>Sydney</td>-->
                <!--<td>Austrália</td>-->
                <!--<td>LOH</td>-->
                <!--<td>vodný slalom - C2</td>-->
                <!--<td><a href="edit.php?chid=1&amp;in=1"><i class="fas fa-pencil-alt"></i></a></td>-->
                <!--<td><a href="index.php?del=1&amp;in=1"><i class="fas fa-ban"></i></a></td>-->
            <!--</tr><tr>-->

            </tbody>
        </table>
       <div class="text-center"><a href="vloz.php" class="btn btn-info text-center" role="button">Pridať záznam do databázy</a>
    </div><br>
    </div></div>


<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>




</body>
</html>