<!DOCTYPE HTML>
<html>
<head>
    <title>Edituj športovca</title>
    <meta charset="UTF-8">
    <style type="text/css">
        label { width: 10em;
            float: left; }
        fieldset { width: 500px; }
        legend { margin: 0 0 0 70%;
            width: 120px;
            text-align:center; }
        select {width: 100px;}
    </style>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="style.css" charset=utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet">
</head>
<body>
<div class="container">
<form action="" class="form-horizontal" method="get">
    <div class="form-group">
        <h2>Edituj športovca</h2>
        <input type="hidden" name="idPer" value=<?php echo $_GET["idPer"]; ?>>

        <input type="hidden" name="index" value=<?php echo $_GET["index"]; ?>>
        <?php
        require "config.php";
        //nacitat config
        $inde = $_GET["index"];
        $indePer = $_GET["idPer"];

        // Create connection
        $conn = new mysqli($serverName, $userName, $password, $dbname);
        $conn->set_charset("utf8");
        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }
        //    $sql = "SELECT umiestnenie.id ,id_person, ID_OH, persons.name, persons.surname, oh.year, oh.city, oh.country, oh.type, discipline, place FROM umiestnenie LEFT JOIN oh ON umiestnenie.ID_OH= oh.id LEFT JOIN persons ON umiestnenie.id_person= persons.id";


        $sql = "SELECT umiestnenie.id ,id_person, ID_OH, persons.name, persons.surname, oh.year, oh.city, oh.country, oh.type, discipline, place FROM umiestnenie LEFT JOIN oh ON umiestnenie.ID_OH= oh.id LEFT JOIN persons ON umiestnenie.id_person= persons.id WHERE ID_OH = '$inde' AND persons.id = '$indePer'";
        $result = $conn->query($sql);
        if ($result->num_rows>0){
            while ($row = $result->fetch_assoc()) {


                echo
                    "<p><label for=\"meno\">Meno</label>
        <input type=\"text\" id=\"meno\" value=". $row["name"] . " name=\"meno\" size=\"40\"></p>
             <p><label for=\"priezvisko\">Priezvisko</label>
        <input type=\"text\" id=\"priezvisko\"  value=". $row["surname"] . " name=\"priezvisko\" size=\"40\"></p>
              <p><label for=\"rok\">Rok konania</label>
        <input type=\"number\" id=\"rok\"  value=". $row["year"] . " name=\"rok\" size=\"40\"></p>
              
              <p><label for=\"mesto\">Mesto konania</label>
        <input type=\"text\" id=\"mesto\"  value=". $row["city"] . " name=\"mesto\" size=\"40\"></p>
              <p><label for=\"krajina\">Krajina konania</label>
        <input type=\"text\" id=\"krajina\"  value=". $row["country"] . " name=\"krajina\" size=\"40\"></p>
              <p><label for=\"typ\">Typ</label>
        <input type=\"text\" id=\"typ\"  value=". $row["type"] . " name=\"typ\" size=\"40\"></p>
                <p><label for=\"discip\">Disciplína</label>
        <input type=\"text\" id=\"discip\"  value=". $row["discipline"] . " name=\"discip\" size=\"40\"></p>
                <p><label for=\"umiestnenie\">Umiestnenie</label>
        <input type=\"number\" id=\"umiestnenie\"  value=". $row["place"] . " name=\"umiestnenie\" size=\"40\">
        ";


            }
        }

        ?>
    </div>

    </p><input type="submit" name="submit" class="btn btn-success" value="Odošli">
</form>
</div>
<?php



if (isset($_GET['submit'])) {
    require "config.php";
//nacitat config
    $inde = $_GET["index"];
    $indePer = $_GET["idPer"];
// Create connection
    $conn = new mysqli($serverName, $userName, $password, $dbname);
    $conn->set_charset("utf8");
// Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }


    $meno = $_GET["meno"];
    $priezvisko = $_GET["priezvisko"];

    $typ = $_GET["typ"];
    $rok = $_GET["rok"];
    $umiestnenie = $_GET["umiestnenie"];
    $mesto = $_GET["mesto"];
    $krajina = $_GET["krajina"];
    $discip = $_GET["discip"];



    $sql = "UPDATE `persons` SET `name` = '$meno', `surname` = '$priezvisko' WHERE  persons.id = '$indePer'";
    if ($conn->query($sql) === TRUE) {
      //  echo "Record updated successfully";
    } else {
       // echo "Error updating record: " . $conn->error;
    }



    $sql = "UPDATE `oh` SET `type`= '$typ', `year` ='$rok', `placement`= '$umiestnenie', `city`= '$mesto', `country`= '$krajina' WHERE oh.id = '$inde' ";
    if ($conn->query($sql) === TRUE) {
       // echo "Record updated successfully";
    } else {
        //echo "Error updating record: " . $conn->error;
    }
    $sql = "UPDATE `umiestnenie` SET `discipline`= '$discip' WHERE  umiestnenie.id_person = '$indePer' AND umiestnenie.ID_OH = '$inde'";
    if ($conn->query($sql) === TRUE) {
        //echo "Record updated successfully";
    } else {
        //echo "Error updating record: " . $conn->error;
    }

    $conn->close();
    echo "<a class=\"btn container\" href='index.php'>Choď späť hlavnu stranku</a>";
}


//$sql = "DELETE FROM (SELECT umiestnenie.id ,id_person AS idPe , ID_OH, persons.name, persons.surname, oh.year, oh.city, oh.country, oh.type, discipline, place FROM umiestnenie LEFT JOIN oh ON umiestnenie.ID_OH= oh.id LEFT JOIN persons ON umiestnenie.id_person= persons.id)
//        AS sporotvci WHERE  sportovci.id = ". $inde ." AND sportovci.idPe = ". $indePer ;



?>
</body>
</html>
