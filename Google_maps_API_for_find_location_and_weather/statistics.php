<!DOCTYPE html>
<html>
<head>
    <title>Štatistika</title>
    <meta charset="utf-8">
    <link type="text/css" rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="CssStyl.css">
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
</head>
<body>
<div class="vybersi">
    <a href="weather.php"> <img class="uvod" src="weather.jpg" alt="pocasie"> </a>
    <a href="place.php"> <img class="uvod" src="miesto.jpg" alt="miesto"> </a>
</div>
<div id="mapy" class="mapa2"></div>
<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once ('config.php');
$pripojenie = new mysqli($hostname, $username, $password, $dbname);
if($pripojenie->connect_error){
    die("Failed to connect with MySQL: " . $conn->connect_error);
}
$ipuzivatela = $_SERVER['REMOTE_ADDR'];
$ch = curl_init('ipinfo.io/'.$ipuzivatela.'/city');
curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
$result = curl_exec($ch);
$ch1 = curl_init('ipinfo.io/'.$ipuzivatela.'/country');
curl_setopt($ch1, CURLOPT_RETURNTRANSFER,true);
$result1 = curl_exec($ch1);
$result1 = trim($result1);
$result2 = json_decode(file_get_contents("https://restcountries.eu/rest/v2/alpha/{$result1}"));
$result1 = strtolower($result1);
$stat = strval($result2->name);
$result = trim($result);
$vlajka = "http://www.geonames.org/flags/x/$result1.gif";
if (empty($result))
    $mesto = "nie je";
else $mesto = $result;
$casovazona = json_decode(file_get_contents("http://ip-api.com/json/$ipuzivatela"));
date_default_timezone_set($casovazona->timezone);
$celydatum= date("r:i A");
$rozdelene=explode(" ",$celydatum);
$insertsql = "INSERT INTO Navstevnici SET Stat='$stat', IP_adresa='$ipuzivatela', Datum='".date("d M Y")."', Link_na_vlajku='$vlajka', Web_stranka='Statistika', Mesto='$mesto', Cas = '".$rozdelene[4]."'";
$pripojenie->query($insertsql);
$sql = "SELECT COUNT(Web_stranka) FROM Navstevnici WHERE Web_stranka = 'Miesto'";
$result3 = $pripojenie->query($sql);
if ($result3->num_rows) {
    while ($row1 = $result3->fetch_row()) {
        foreach ($row1 as $key => $val1)
            echo "<div class='jednodiv'>Prihlásilo sa ".$val1." zo stránky miesto.php<br>";
    }
}
$sql = "SELECT COUNT(Web_stranka) FROM Navstevnici WHERE Web_stranka = 'Statistika'";
$result4 = $pripojenie->query($sql);
if ($result4->num_rows) {
    while ($row2 = $result4->fetch_row()) {
        foreach ($row2 as $key => $val2)
            echo "Prihlásilo sa ".$val2." zo stránky statistika.php<br>";
    }
}
$sql = "SELECT COUNT(Web_stranka) FROM Navstevnici WHERE Web_stranka = 'Pocasie'";
$result4 = $pripojenie->query($sql);
if ($result4->num_rows) {
    while ($row3 = $result4->fetch_row()) {
        foreach ($row3 as $key => $val3)
            echo "Prihlásilo sa ".$val3." zo stránky pocasie.php<br>";
    }
}
$stranky = array("place.php" => $val1, "statistics.php" => $val2, "weather.php" => $val3);
foreach($stranky as $x => $x_value) {
    if ($x_value == max($val1,$val2,$val3)) {
        $stranka = $x;
        break;
    }
}
echo "Najčastejšie navštevovaná stránka je: ".$stranka."</div>";
$sql = "SELECT DISTINCT(Stat), Link_na_vlajku FROM Navstevnici";
$result5 = $pripojenie->query($sql);
$statyvlajky = array();
if ($result5->num_rows) {
    while ($row4 = $result5->fetch_row()) {
        foreach ($row4 as $key => $val4)
            array_push($statyvlajky,$val4);
    }
}
$sql = "SELECT Stat, COUNT(IP_adresa) FROM (SELECT Stat, Datum, IP_adresa, COUNT(ID) FROM Navstevnici GROUP BY Stat, Datum, IP_adresa) AS Unikat GROUP BY Stat";
$result6 = $pripojenie->query($sql);
echo "<table class='tabulka'><tr><th>Štát</th><th>Vlajka</th><th>Počet návštevníkov</th></tr>";
if ($result6->num_rows) {
    while ($row5 = $result6->fetch_row()) {
        echo "<tr>";
        foreach ($row5 as $key => $val5) {
            echo "<td><a href=\"mestecko.php?Stat=". $val5." \">". $val5."</a></td>";
            for ($l = 0; $l < count($statyvlajky);$l = $l + 2){
                if ($val5 == $statyvlajky[$l]) {
                    echo "<td>". '<img class=\'obrazok\' src='.$statyvlajky[$l + 1].'></td>';
                    break;
                }
            }
        }
        echo "</tr>";
    }
    echo "</table><br>";
}
$sql = "SELECT Cas FROM Navstevnici";
$result7 = $pripojenie->query($sql);
$inetval1 = 0;
$inetval2 = 0;
$inetval3 = 0;
$inetval4 = 0;
if ($result7->num_rows) {
    echo "<table class='tabulka'><tr><th>06:00-13:59</th><th>14:00-19:59</th><th>20:00-23:59</th><th>00:00-05:59</th></tr>";
    while ($row6 = $result7->fetch_row()) {
        foreach ($row6 as $key => $val6) {
            $datum = explode(":",$val6);
            $datum[0] = intval($datum[0]);
            $datum[1] = intval($datum[1]);
            $datum[2] = intval($datum[2]);
            if($datum[0] >= 6 && $datum[0] <= 13 && $datum[1] >= 0 && $datum[1]  <= 59)
                $inetval1++;
            if($datum[0] >= 14 && $datum[0] <= 19 && $datum[1] >= 0 && $datum[1]  <= 59)
                $inetval2++;
            if($datum[0] >= 20 && $datum[0] <= 23 && $datum[1] >= 0 && $datum[1]  <= 59)
                $inetval3++;
            if($datum[0] >= 0 && $datum[0] <= 5 && $datum[1] >= 0 && $datum[1]  <= 59)
                $inetval4++;
        }
    }
    echo "<tr><td>$inetval1</td><td>$inetval2</td><td>$inetval3</td><td>$inetval4</td></tr></table><br>";
}

?>
<div id="tabstat"></div>
<script type="text/javascript" src="JAVAscript.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAQuZbyoS6SkZ_EBN9VolqTcsIvX1AVOE4&callback=mojaMapa&libraries=places" async defer></script>
</body>
</html>