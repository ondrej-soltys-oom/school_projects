var suradnice = $("#suradnice").html();
if (suradnice == undefined){
    $.ajax({
        type: "POST",
        url: "vsetkymesta.php",
        success: function (vratene) {
            var vratene2 = JSON.parse(vratene);
            var addressArray= new Array();
            for(var j=0;j<vratene2.length;j++)
            {
                if(vratene2[j]!="nie je")
                {
                    addressArray.push(vratene2[j]);
                }
            }
            var myOptions = {
                center: new google.maps.LatLng(54, -2),
                zoom: 6,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            var map = new google.maps.Map(document.getElementById("mapy"), myOptions);
            var geocoder = new google.maps.Geocoder();
            var markerBounds = new google.maps.LatLngBounds();
            for (var i = 0; i < addressArray.length; i++) {
                geocoder.geocode( { 'address': addressArray[i]}, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        var marker = new google.maps.Marker({
                            map: map,
                            position: results[0].geometry.location
                        });
                        markerBounds.extend(results[0].geometry.location);
                        map.fitBounds(markerBounds);
                    } else {
                        alert("Geocode was not successful for the following reason: " + status);
                    }
                });
            }
        }
    })
}
else {
    var surkon = suradnice.split(",");
    surkon[0] = surkon[0].substr(11);
    function mojaMapa() {
        var mesto = {lat: parseFloat(surkon[0]), lng: parseFloat(surkon[1])};
        var mapa = new google.maps.Map(document.getElementById('mapy'), {
            zoom: 10,
            center: mesto
        });
        var marker = new google.maps.Marker({
            position: mesto,
            map: mapa
        });
    }
}
$("tr td:first-child").click(function() {
    var jedenstat = $(this).html();
    $.ajax({
        type: "POST",
        url: "tabul.php",
        data: {'Statjeden' : jedenstat},
        success: function (vratene) {
            var vratene2 = JSON.parse(vratene);
            $("#tabstat").html("<table class='table'><tr><th>Mesto</th><th>Počet návštevníkov</th></tr>");
            for (var t = 0; t < vratene2.length;t = t+3) {
                if (vratene2[t] == "nie je")
                    vratene2[t] = "nelokalizované mestá a vidiek";
                $(".table").append("<tr><td>" + vratene2[t] + "</td><td>" + vratene2[t+2] + "</td></tr>");
            }
            $("#tabstat").append("</table>");
        }
    });
});