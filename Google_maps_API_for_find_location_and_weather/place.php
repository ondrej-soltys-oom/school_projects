<!DOCTYPE html>
<html>
<head>
    <title>Geolokácia</title>
    <meta charset="utf-8">
    <link type="text/css" rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="CssStyl.css">
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
</head>
<body>
<div class="vyber">
    <a href="weather.php"> <img class="uvod pocas" src="weather.jpg" alt="pocasie"> </a>
    <a href="statistics.php"> <img class="uvod dalej" src="statistika.jpg" alt="statistika"> </a>
</div>
<div class="stred">
    <?php
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
    require_once ('config.php');
    $pripojenie = new mysqli($hostname, $username, $password, $dbname);
    if($pripojenie->connect_error){
        die("Failed to connect with MySQL: " . $pripojenie->connect_error);
    }
    $userip = $_SERVER['REMOTE_ADDR'];
    $ch = curl_init('ipinfo.io/'.$userip.'/ip');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
    $result = curl_exec($ch);
    curl_close($ch);
    $ch2 = curl_init('ipinfo.io/'.$userip.'/loc');
    curl_setopt($ch2, CURLOPT_RETURNTRANSFER,true);
    $result2 = curl_exec($ch2);
    curl_close($ch2);
    $ch3 = curl_init('ipinfo.io/'.$userip.'/city');
    curl_setopt($ch3, CURLOPT_RETURNTRANSFER,true);
    $result3 = curl_exec($ch3);
    curl_close($ch3);
    $ch4 = curl_init('ipinfo.io/'.$userip.'/country');
    curl_setopt($ch4, CURLOPT_RETURNTRANSFER,true);
    $result4 = curl_exec($ch4);
    curl_close($ch4);
    echo "<div class='jednodiv'>IP: ".$result."<br>";
    echo "<span id='suradnice'>Súradnice: ".$result2."</span><br>";
    $result3 = trim($result3);
    if (empty($result3)) {
        echo "Mesto sa nedá lokalizovať alebo sa nachádzate na vidieku<br>";
        $result3 = "nie je";
    }
    else echo "Mesto: ".$result3."<br>";
    $result4 = trim($result4);
    $result5 = json_decode(file_get_contents("https://restcountries.eu/rest/v2/alpha/{$result4}"));
    echo "Štát: ".strval($result5->name)."<br>";
    echo "Hlavné mesto: ".strval($result5->capital)."</div>";
    $stat = strval($result5->name);
    $mesto = $result3;
    $result4 = strtolower($result4);
    $vlajka = "http://www.geonames.org/flags/x/$result4.gif";
    $casovazona = json_decode(file_get_contents("http://ip-api.com/json/$userip"));
    date_default_timezone_set($casovazona->timezone);
    $celydatum= date("r:i A");
    $rozdelene=explode(" ",$celydatum);
    $sql= "INSERT INTO Navstevnici SET Stat = '".$stat."', IP_adresa = '".$userip."', Datum = '".date("d M Y")."', Link_na_vlajku = '".$vlajka."', Web_stranka='Miesto', Mesto = '".$mesto."', Cas = '".$rozdelene[4]."'";
    $results = $pripojenie->query($sql);
    ?>
    <div id="mapy" class="mapa1"></div>
    <script type="text/javascript" src="JAVAscript.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAQuZbyoS6SkZ_EBN9VolqTcsIvX1AVOE4&callback=mojaMapa&libraries=places" async defer></script>
</div>
</body>
</html>
