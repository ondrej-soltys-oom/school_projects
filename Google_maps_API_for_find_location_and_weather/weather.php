<!DOCTYPE html>
<html>
<head>
    <title>Počasie</title>
    <meta charset="utf-8">
    <link type="text/css" rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="CssStyl.css">
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
</head>
<body>
<div class="vyber">
    <a href="place.php"> <img class="uvod" src="miesto.jpg" alt="miesto"> </a>
    <a href="statistics.php"> <img class="uvod" src="statistika.jpg" alt="statistika"> </a>
</div>
<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once ('config.php');
$pripojenie = new mysqli($hostname, $username, $password, $dbname);
if($pripojenie->connect_error){
    die("Failed to connect with MySQL: " . $pripojenie->connect_error);
}
$ipuzivatela = $_SERVER['REMOTE_ADDR'];
$ch = curl_init('ipinfo.io/'.$ipuzivatela.'/city');
curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
$result = curl_exec($ch);
$ch1 = curl_init('ipinfo.io/'.$ipuzivatela.'/country');
curl_setopt($ch1, CURLOPT_RETURNTRANSFER,true);
$result1 = curl_exec($ch1);
$result1 = trim($result1);
$result2 = json_decode(file_get_contents("https://restcountries.eu/rest/v2/alpha/{$result1}"));

$result1 = strtolower($result1);
$stat = strval($result2->name);
$result = trim($result);
$vlajka = "http://www.geonames.org/flags/x/$result1.gif";
if (empty($result))
    $mesto = "nie je";
else $mesto = $result;
$casovazona = json_decode(file_get_contents("http://ip-api.com/json/$ipuzivatela"));

date_default_timezone_set($casovazona->timezone);
$celydatum= date("r:i A");
$rozdelene=explode(" ",$celydatum);
$insertsql = "INSERT INTO Navstevnici SET Stat='$stat', IP_adresa='$ipuzivatela', Datum='".date("d M Y")."', Link_na_vlajku='$vlajka', Web_stranka='Pocasie', Mesto='$mesto', Cas = '".$rozdelene[4]."'";
$pripojenie->query($insertsql);
$ch = curl_init('ipinfo.io/'.$ipuzivatela.'/city');
curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
$result = curl_exec($ch);
curl_close($ch);

$pocasie = json_decode(file_get_contents('http://api.openweathermap.org/data/2.5/forecast/daily?q='.trim($result).'&units=metric&cnt=3&appid=2f73b483e033f431a3fc61b0a73f2b5c'));


$result = trim($result);
if (empty($result))
    echo "Mesto sa nedá lokalizovať alebo sa nachádzate na vidieku a preto nie je možné určiť počasie<br>";
else {
    echo "<div class='pocasie1'>Predpoveď počasia na 3 dni<br>";
    echo "Počasie pre mesto: " . $result . "</div><table class='table table-condensed'><tr><th>Deň</th><th>Teplota minimálna</th><th>Teplota maximálna</th><th>Teplota cez noc</th><th>Tlak</th><th>Vlhkosť vzduchu</th><th>Rýchlosť vetra</th><th>Popis</th></tr>";
    $dni = array("Dnes", "Zajtra", "Pozajtra");
    for ($i = 0; $i < 3; $i++) {
        echo "<tr><td>" . $dni[$i] . "</td>";
        echo "<td>" . $pocasie->list[$i]->temp->min . " °C</td>";
        echo "<td>" . $pocasie->list[$i]->temp->max . " °C</td>";
        echo "<td>" . $pocasie->list[$i]->temp->night . " °C</td>";
        echo "<td>" . $pocasie->list[$i]->pressure . " hPa</td>";
        echo "<td>" . $pocasie->list[$i]->humidity . " %</td>";
        echo "<td>" . $pocasie->list[$i]->speed . " m/s</td>";
        echo "<td>" . $pocasie->list[$i]->weather[0]->description . "</td></tr>";
    }
    echo "</table>";
}
?>
</body>
</html>