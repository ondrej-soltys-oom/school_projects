<?php
require_once ('config.php');
$pripojenie = new mysqli($hostname, $username, $password, $dbname);
if($pripojenie->connect_error){
    die("Failed to connect with MySQL: " . $pripojenie->connect_error);
}
$sql = "SELECT DISTINCT Mesto FROM Navstevnici";
$result = $pripojenie->query($sql);
$vratene = [];
if ($result->num_rows) {
    while ($row = $result->fetch_row()) {
        foreach ($row as $key => $val)
            array_push($vratene,$val);
    }
}
echo json_encode($vratene);
?>