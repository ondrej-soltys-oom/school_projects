<!DOCTYPE html>
<html lang="sk">
<head>
    <meta charset="utf-8">
    <title>Prihlásenie</title>

    <link type="text/css" rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>
<body>

<div class="container">
    <form action="" method="get">
        <div class="col-md-6">
            <h2>Registrácia</h2>
            <p><label for="name">Meno</label>
                <input class="form-control" type="text" id="name" name="name" size="40"></p>
            <p><label for="surname">Priezvisko</label>
                <input class="form-control" type="text" id="surname" name="surname" size="40"></p>
            <p><label for="mail">Email</label>
                <input class="form-control" type="email" id="mail" name="mail" size="40"></p>
            <p><label for="log">Login</label>
                <input class="form-control" type="text" id="log" name="log" size="40"></p>
            <p><label for="pass">Heslo</label>
                <input class="form-control" type="password" id="pass" name="pass" size="40"></p>
            <p><label for="passCheck">Zopakuj heslo</label>
                <input class="form-control" type="password" id="passCheck" name="passCheck" size="40"></p>


            <input type="submit" name="submit" value="Registruj">
        </div>
    </form>
</div>

<?php
if (isset($_GET['submit'])) {
    require "config.php";
//nacitat config

// Create connection
    $conn = new mysqli($serverName, $userName, $password, $dbname);
    $conn->set_charset("utf8");
// Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }


    $name = $_GET["name"];
    $surname = $_GET["surname"];
    $mail = $_GET["mail"];
    $login = $_GET["log"];
    $password = $_GET["pass"];
    $confirmpassword = $_GET["passCheck"];
    if ($password != $confirmpassword) {
        echo("Error... Passwords do not match <br>");
        $conn->close();
        echo "<a class=\"btn container\" href='index.php'>Choď späť hlavnu stranku</a>";
    } else {
        $date = date("Y-m-d");
        $passwordHash = password_hash($password, PASSWORD_BCRYPT);

        $sql = "INSERT INTO `uzivatel` 
                  (`id`, `registration`, `name`, `surname`, `mail`, `login`, `password`) 
              VALUES (NULL,  '$date','$name', '$surname', '$mail', '$login', '$passwordHash')";


        if ($conn->query($sql) === TRUE) {
            // echo "New record created successfully";
        } else {
            //  echo "Error: " . $sql . "<br>" . $conn->error;
        }

    }


    $conn->close();
    echo "<a class=\"btn container\" href='index.php'>Choď späť hlavnu stranku</a>";
}


?>


</body>
</html>