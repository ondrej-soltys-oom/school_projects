<!DOCTYPE html>
<html lang="sk">
<head>
    <meta charset="utf-8">
    <title>Prihlásenie</title>

    <link type="text/css" rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>
<body>
<div class=" container">
    <div class="row col-md-6 ">
        <div ><br>

            <div >
                <form action="LDAP.php" method="post">
                    <fieldset class="group-border">
                        <legend class="group-border">Prihásiť sa cez LDAP</legend>
                        <div class="form-group">
                            <label>Login</label>
                            <input class="form-control" name="menoLDAP" required type="text" placeholder="Login">
                        </div>
                        <div class="form-group">
                            <label>Heslo</label>
                            <input class="form-control" name="hesloLDAP" required type="password" placeholder="Heslo">
                        </div>
                        <input class="btn btn-primary" type="submit" value="Prihlásiť LDAP">
                    </fieldset>
                </form>
            </div>
        </div>
        <div ><br>
        <form action="login.php" method="post">
            <fieldset class="group-border">
                <legend class="group-border">Prihásiť sa cez registraciu</legend>
                <div>

                    <input  class="form-control" type="text" name="meno" placeholder="Login">
                </div>
                <div>

                    <input  class="form-control" type="password" name="heslo" placeholder="Heslo">
                </div>
            </fieldset><br>
            <input type="submit" value="Prihlasit registracia"><br>
        </form>
        </div ><br>
        <div ><a href="registration.php" class="btn btn-info text-center" role="button">Registrácia</a>
        </div><br>
        <div  >
            <?php
            session_start();
            //Include GP config file && User class
            include_once 'GoogleAPI.php';



            if (isset($_GET['code'])) {
                $gClient->authenticate($_GET['code']);
                $_SESSION['token'] = $gClient->getAccessToken();
                header('Location: ' . filter_var($redictUrl, FILTER_SANITIZE_URL));
            }

            if (isset($_SESSION['token'])) {
                $gClient->setAccessToken($_SESSION['token']);
            }

            if ($gClient->getAccessToken()) {
                //Get user profile data from google
                $gpUserProfile = $google_oauthV2->userinfo->get();

                //Initialize User class
                //$user = new User();

                //Insert or update user data to the database
                $gpUserData = array(
                    'oauth_provider' => 'google',
                    'oauth_uid' => $gpUserProfile['id'],
                    'first_name' => $gpUserProfile['given_name'],
                    'last_name' => $gpUserProfile['family_name'],
                    'email' => $gpUserProfile['email'],
                    'gender' => $gpUserProfile['gender'],
                    'locale' => $gpUserProfile['locale'],
                    'picture' => $gpUserProfile['picture'],
                    'link' => $gpUserProfile['link']
                );
                $userData = $gpUserData;


                //Storing user data into session
                $_SESSION['userData'] = $userData;

                header("Location:tajne.php");

            } else {
                $authUrl = $gClient->createAuthUrl();

                $output = '<a href="'.filter_var($authUrl, FILTER_SANITIZE_URL).'"><img src="images/glogin.png" alt=""/></a>';
                echo $output;
            }


            ?>
        </div>
    </div>



<!--    <a href="tajne.php">Tajny dokument</a><br>-->
<!--    <a href="logout.php">Odhlasit</a>-->
<!--    <a href="loginGoogle.php">ahoj</a>-->
    <!--    -->
</div>
</body>
</html>