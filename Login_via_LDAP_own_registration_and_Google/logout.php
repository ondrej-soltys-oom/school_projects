<?php
session_start();

if (!isset($_SESSION['User'])) {
    echo "Naco sa odhlasujete, ked ste sa neprihlasili.<br>\n";
    echo "</body>\n</html>\n";
    exit;
}
echo $_SESSION['User'] . ", majte sa pekne, logout.<br>\n";
unset($_SESSION['User']);
session_destroy();
header("Location:index.php");
?>
