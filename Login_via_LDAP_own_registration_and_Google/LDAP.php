<?php
session_start();
if (!empty($_POST['menoLDAP']) && !empty($_POST['hesloLDAP'])) {
    require "config.php";
    $ldapuid = $_POST['menoLDAP'];
    $ldappass = $_POST['hesloLDAP'];
    $_SESSION['login'] = $ldapuid;
    $dn = 'ou=People, DC=stuba, DC=sk';
    $ldaprdn = "uid=$ldapuid, $dn";
    $ldapconn = ldap_connect("ldap.stuba.sk") or die("Nie je možné sa pripojiť na LDAP server");

    ldap_set_option($ldapconn, LDAP_OPT_PROTOCOL_VERSION, 3);

    $ldapBindResult = ldap_bind($ldapconn, $ldaprdn, $ldappass);
    if ($ldapBindResult) {

        $sr = ldap_search($ldapconn, $ldaprdn, "uid=$ldapuid");
        $entry = ldap_first_entry($ldapconn, $sr);
        $usrId = ldap_get_values($ldapconn, $entry, "uisid")[0];
        $usrName = ldap_get_values($ldapconn, $entry, "givenname")[0];
        $usrSurname = ldap_get_values($ldapconn, $entry, "sn")[0];
        $usrMail = ldap_get_values($ldapconn, $entry, "mail")[0];
        $udajeLDAP = $usrName . ' ' . $usrSurname;
        $_SESSION['LDAP'] = $udajeLDAP;


        //$_SESSION['array'] = $info;

        $conn = new mysqli($serverName, $userName, $password, $dbname);
        $conn->set_charset("utf8");

        if ($conn->connect_error) {
            die("Failed to connect with MySQL: " . $conn->connect_error);
        }

        $prevQuery = "SELECT * FROM uzivatel WHERE mail = '" . $usrMail . "'";
        $prevResult = $conn->query($prevQuery);

        if ($prevResult->num_rows > 0) {
            while ($row = $prevResult->fetch_assoc()) {

                $person_id = $row["id"];
            }
            //Update user data if already exists
            $query = "INSERT INTO prihlasenie SET id_uzivatel = '" . $person_id . "', method = '" . "LDAP" . "', time = '" . date("H:i:s") . "'";
            if ($conn->query($query) === TRUE) {
               // echo "New record created successfully";
            } else {
                echo "Error: " . $query . "<br>" . $conn->error;
            }
            $_SESSION['id'] = $person_id;
        } else {

            //Insert user data
            $query2 = "INSERT INTO uzivatel SET  name = '" . $usrName . "', surname = '" . $usrSurname . "', mail = '" . $usrMail . "',  registration = '" . date("Y-m-d") . "'";
            if ($conn->query($query2) === TRUE) {
                // echo "New record created successfully";
            } else {
                echo "Error: " . $query2 . "<br>" . $conn->error;
            }
            $person_id = $conn->insert_id;
            $query3 = "INSERT INTO prihlasenie SET id_uzivatel = '" . $person_id . "', method = '" . "LDAP" . "', time = '" . date("H:i:s") . "'";
            if ($conn->query($query3) === TRUE) {
                // echo "New record created successfully";
            } else {
                echo "Error: " . $query3 . "<br>" . $conn->error;
            }
            $_SESSION['id'] = $person_id;
        }

        $conn->close();
        header("Location: tajne.php");
    } else {
        header("Location:index.php");
    }
    @ldap_close($ldapconn);
}
?>