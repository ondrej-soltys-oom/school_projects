<?php
session_start();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Prihlásenia</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
<!--    --<link rel="stylesheet" type="text/css" href="style.css" charset=utf-8">-->

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet">
</head>
<body>


<div class="container">
    <p class="h1 text-center">Prihlaseny</p>
    <?php
    if(isset($_SESSION['name']))
    {   $meno = $_SESSION['name'];
        echo "<p class=\"h1 text-center\">". $meno ."</p>";
    }
    ?>
    <br>
    <div class="table-responsive">
        <table class="table table-striped table-hover">
            <thead class="thead-dark">
            <tr>
                <th>Meno</th>
                <th>Priezvisko</th>
                <th>Spôsob prihlásenia</th>
                <th>Cas</th>

            </tr>
            </thead>
            <tbody>
            <?php

            require "config.php";
            //nacitat config
            $id = $_SESSION['id'];

            // Create connection
            $conn = new mysqli($serverName, $userName, $password, $dbname);
            $conn->set_charset("utf8");
            // Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }
            $sql = "SELECT * FROM `prihlasenie` JOIN uzivatel  on prihlasenie.id_uzivatel = uzivatel.id WHERE  prihlasenie.id_uzivatel = '$id'";
            $result = $conn->query($sql);


            if ($result->num_rows>0){
                while ($row = $result->fetch_assoc()) {


                    echo "<tr>" .
                        "<td>" . $row["name"] . "</td>" .
                        "<td>" . $row["surname"] . "</td>" .
                        "<td>" . $row["method"] . "</td>" .
                        "<td>" . $row["time"] . "</td>" .
                        "</tr>" ;

                }
            }


            ?>
            <tr>
                <th>Registrácia</th>
                <th>LDAP</th>
                <th>GOOGLE</th>


            </tr>
            <?php

            require "config.php";
            //nacitat config
            $id = $_SESSION['id'];

            // Create connection
            $conn = new mysqli($serverName, $userName, $password, $dbname);
            $conn->set_charset("utf8");
            // Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }
            $sql = "SELECT (SELECT COUNT(*) as reg FROM prihlasenie WHERE prihlasenie.method = \"registracia\") as reg, (SELECT COUNT(*) as ldap FROM prihlasenie WHERE prihlasenie.method = \"LDAP\") as ldap , (SELECT COUNT(*) as google FROM prihlasenie WHERE prihlasenie.method = \"google\") as google";
            $result = $conn->query($sql);


            if ($result->num_rows>0){
                while ($row = $result->fetch_assoc()) {


                    echo "<tr>" .
                        "<td>" . $row["reg"] . "</td>" .
                        "<td>" . $row["ldap"] . "</td>" .
                        "<td>" . $row["google"] . "</td>" .

                        "</tr>" ;

                }
            }


            ?>
            </tbody>
        </table>
        <div class="text-center"><a href="tajne.php" class="btn btn-info text-center" role="button">Späť</a>
        </div><br>
    </div></div>


<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>




</body>
</html>