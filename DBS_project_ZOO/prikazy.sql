-- Vlozenie novej adresy
INSERT INTO `adresa` (`ID_ADRESA`, `Ulica`, `Cislo_domu`, `PSC`, `Mesto`,`Krajina`) VALUES (NULL, 'Plynárenská', '98', '94111', 'Banská Štiavnica','Slovenská Republika');

-- Vlozenie noveho dodavatela
INSERT INTO `dodavatel` (`ID_DODAVATEL`, `Nazov`, `ID_ADRESA`, `Telefon`,`ICO`, `DIC`) VALUES (NULL, 'Horales s.r.o.', '8', '+420958787444','CZ166561651165', 'CZ1745-416-419');

-- Vlozenie noveho krmica
INSERT INTO `krmic` (`ID_KRMIC`, `Meno`, `Priezvisko`, `Titul`, `Plat`,`Vzdelanie`, `Osobny_telefon`, `Nudzovy_telefon`, `Datum_nastupu`,`ID_ADRESA`, `Rodne_cislo`, `Cislo_uctu`) VALUES (NULL, 'Anton', 'Srnka','Mgr.', '7.2', 'University in Bearlky, New York', '+421123456785','+421525489623', '2017-05-22', '7', '06081985/2755','SK061656516561516656565651');

-- Vlozenie noveho planu krmenia
INSERT INTO `plan_krmenia` (`ID_PLAN_KRMENIA`, `ID_POTRAVA`, `ID_KRMIC`,`Cas`, `Mnozstvo`, `Aktivny`) VALUES (NULL, '2', '2', '14:00:28', '700', '1');

-- Vlozenie novej potravy
INSERT INTO `potrava` (`ID_POTRAVA`, `ID_DODAVATEL`, `Nazov`, `Zasoby`,`Trvanlivost`, `Uskladnenie`) VALUES (NULL, '5', 'Paleta obyčaná', '5000000','2018-05-18', 'Sucho');

-- Vlozenie novych rodicov
INSERT INTO `rodicia` (`ID_RODICOV`, `ID_OTEC`, `ID_MATKA`, `Meno_otca`,`Meno_matky`) VALUES (NULL, NULL, NULL, 'Palalate', 'Pokana');

-- Vlozenie spotreby krmiva
INSERT INTO `spotreba_krmiva` (`ID_SPOTREBA`, `ID_PLAN_KRMENIA`, `Datum_cas`,`Koniec`) VALUES (NULL, '3', '2017-05-24 07:00:00', '2017-05-24 09:00:00');

-- Vlozenie noveho zvierata
INSERT INTO `zviera` (`ID_ZVIERA`, `Meno`, `Pohlavie`, `Druh`,`Datum_narodenia`, `Stav`, `Spravanie`, `ID_RODICIA`) VALUES (NULL, 'Afalero','Samec', 'Abimiec nubijský', '2017-05-02 08:00:00', '1', NULL, NULL);

-- Dotaz pre zistenie počtu všetkých zvierat v zoo. 

SELECT COUNT(*) as "Počet zvierat" FROM `zviera` WHERE Stav = 1;

-- Dotaz pre zistenie počtu všetkých kŕmičov v ZOO 

SELECT COUNT(*) as "Počet kŕmičov" FROM `krmic`;

-- Dotaz pre výpis najviac platených kŕmičov 

SELECT ID_KRMIC as "Identifikačné číslo", CONCAT(Meno," ",Priezvisko) as "Meno a priezvisko", (Plat/100) as "Plat" FROM `krmic` ORDER BY Plat DESC;

-- Dotaz pre výpis najpracovitejšieho kŕmiča (najviac pracovných hodín za týždeň) 

SELECT k.ID_KRMIC as "Identifikacne cislo", CONCAT(k.Meno," ",k.Priezvisko) as "Meno krmica",SUM(TIME_TO_SEC(TIMEDIFF(sk.Koniec,sk.Datum_cas)))/3600 as Odpracovane_hodiny FROM spotreba_krmiva sk
JOIN plan_krmenia pk ON pk.ID_PLAN_KRMENIA = sk.ID_PLAN_KRMENIA
JOIN krmic k ON k.ID_KRMIC = pk.ID_KRMIC
WHERE YEAR(sk.Datum_cas) = YEAR(NOW()) AND WEEK(sk.Datum_cas) = WEEK(NOW())
GROUP BY pk.ID_KRMIC
ORDER BY Odpracovane_hodiny DESC
LIMIT 0,1;


-- Dotaz pre zistenie najnižšej mzdy kŕmiča 

SELECT Plat as "Najnižší Plat" FROM `krmic` ORDER BY Plat ASC LIMIT 0,1;

-- Dotaz pre výpis súčtu spotrebovaných krmív pre daný druh 

SELECT z.Druh, p.Nazov,COUNT(pk.ID_PLAN_KRMENIA) as Pocet_krmeni, pk.Mnozstvo*COUNT(pk.ID_PLAN_KRMENIA) as "Spotrebovane(g)" FROM zviera z
JOIN plan_zvierata pz ON pz.ID_ZVIERA = z.ID_ZVIERA
JOIN plan_krmenia pk ON pk.ID_PLAN_KRMENIA = pz.ID_PLAN_KRMENIA
JOIN potrava p ON p.ID_POTRAVA = pk.ID_POTRAVA
JOIN spotreba_krmiva sp ON sp.ID_PLAN_KRMENIA = pz.ID_PLAN_KRMENIA
WHERE Druh = "Alopex Lagopus"
GROUP BY p.Nazov;

-- Dotaz  pre výpis spotrebovaných krmív pre daný druh, mesačné rozdelenie

SELECT Year(sp.Datum_cas) as Rok,Month(sp.Datum_cas) as Mesiac,z.Druh, p.Nazov,COUNT(pk.ID_PLAN_KRMENIA) as Pocet_krmeni, pk.Mnozstvo*COUNT(pk.ID_PLAN_KRMENIA) as "Spotrebovane(g)" FROM zviera z
JOIN plan_zvierata pz ON pz.ID_ZVIERA = z.ID_ZVIERA
JOIN plan_krmenia pk ON pk.ID_PLAN_KRMENIA = pz.ID_PLAN_KRMENIA
JOIN potrava p ON p.ID_POTRAVA = pk.ID_POTRAVA
JOIN spotreba_krmiva sp ON sp.ID_PLAN_KRMENIA = pz.ID_PLAN_KRMENIA
WHERE Druh = "Alopex Lagopus"
GROUP BY Year(sp.Datum_cas),Month(sp.Datum_cas),p.Nazov
ORDER BY Rok DESC ,Mesiac DESC;


-- Dotaz ktorý vypíše všetky druhy zvierat a k nim ich počet

Select Distinct(Zviera.Druh) as Druh, count(Zviera.ID_zviera) as pocet From Zviera Group by Druh ORDER BY pocet DESC;

-- Dotaz pre výpis prehľadu všetkých zvierat v zoo v rátane výpisu druhu, počtu kusov daného druhu, potravy daného druhu a denného množstva, ktoré skonzumuje každý jedinec daného druhu. 

SELECT z.Druh,Count(DISTINCT(z.ID_ZVIERA)) as "Pocet jedincov",p.Nazov,SUM(pk.Mnozstvo)/Count(DISTINCT(z.ID_ZVIERA)) as "Mnozstvo pre jedinca" FROM plan_krmenia pk
JOIN plan_zvierata pz on pk.ID_PLAN_KRMENIA = pz.ID_PLAN_KRMENIA
LEFT JOIN zviera z ON z.ID_ZVIERA = pz.ID_ZVIERA
JOIN potrava p ON p.ID_POTRAVA = pk.ID_POTRAVA
GROUP BY pk.ID_POTRAVA, z.Druh;



-- Dotaz pre výpis mena a správania napr. najnovšieho zvieracieho člena 

SELECT Meno, Spravanie FROM `zviera` ORDER BY Datum_narodenia DESC LIMIT 0,1;

-- Dotaz pre výpis všetkých kŕmičov a ich plat a vzdelanie 

SELECT ID_KRMIC as "Identifikačné číslo",Titul, CONCAT(Meno," ",Priezvisko) as "Meno a priezvisko", Vzdelanie, Plat as "Plat" FROM `krmic`;

-- Dotaz pre výpis výdavkov minutých na mzdu kŕmičov 

SELECT pk.ID_KRMIC as "Identifikacne cislo", CONCAT(k.Titul,k.Meno," ",k.Priezvisko) as "Meno a priezvisko",Year(sk.Datum_cas) as Rok,Month(sk.Datum_cas) as Mesiac,SUM(pk.ID_KRMIC) as Pocet_krmeni, (SUM(TIME_TO_SEC(TIMEDIFF(sk.Koniec,sk.Datum_cas)))/3600)*k.Plat as "Mesacná mzda" FROM spotreba_krmiva sk
JOIN plan_krmenia pk ON pk.ID_PLAN_KRMENIA = sk.ID_PLAN_KRMENIA
JOIN krmic k ON k.ID_KRMIC = pk.ID_KRMIC
WHERE 1
GROUP BY Year(sk.Datum_cas),Month(sk.Datum_cas),pk.ID_KRMIC
ORDER BY pk.ID_KRMIC,Rok,Mesiac ASC;


-- Dotaz pre výpis všetkých plánov kŕmenia 

SELECT pk.ID_PLAN_KRMENIA "Idenifikátor", CONCAT(k.Meno," ",k.Priezvisko) as "Krmic", p.Nazov as "Potrava", pk.Mnozstvo as "Mnozstvo na jedinca", pk.Cas FROM plan_krmenia pk
JOIN potrava p ON p.ID_POTRAVA = pk.ID_POTRAVA
JOIN krmic k ON k.ID_KRMIC = pk.ID_KRMIC
WHERE pk.Aktivny = 1;


-- Dotaz pre výpis plánu kŕmenia (pre konkretný druh) a celé meno kŕmiča ktorý má na starosti tento plán 
SELECT CONCAT(k.Meno," ",k.Priezvisko) as "Meno krmica",z.Druh as "Druh zvierata", pk.Cas as "Cas krmenia", p.Nazov as "Potrava", pk.Mnozstvo*COUNT(DISTINCT(z.ID_ZVIERA)) as "Mnozstvo potravy" FROM zviera z
JOIN plan_zvierata pz ON pz.ID_ZVIERA = z.ID_ZVIERA
JOIN plan_krmenia pk ON pk.ID_PLAN_KRMENIA = pz.ID_PLAN_KRMENIA
JOIN krmic k ON k.ID_KRMIC = pk.ID_KRMIC
JOIN potrava p ON p.ID_POTRAVA = pk.ID_POTRAVA
WHERE z.Druh = "Alopex Lagopus"
GROUP BY pk.ID_PLAN_KRMENIA;

-- Dotaz pre výpis statistiky o mnozstve potravy spotrebovanej na krmenie podla druhu potravy a daného druhu zvierata za obdobie (napr. rok) 

SELECT p.Nazov,SUM(pk.Mnozstvo)*COUNT(sp.ID_SPOTREBA)/1000 as "Spotrebovane množstvo" FROM spotreba_krmiva sp
JOIN plan_krmenia pk ON pk.ID_PLAN_KRMENIA = sp.ID_PLAN_KRMENIA
JOIN potrava p ON p.ID_POTRAVA = pk.ID_POTRAVA
JOIN plan_zvierata pz ON pz.ID_PLAN_KRMENIA = pk.ID_PLAN_KRMENIA
JOIN zviera z ON z.ID_ZVIERA = pz.ID_ZVIERA
WHERE z.Druh = "Kengura červená" AND YEAR(sp.Datum_cas) = 2017
GROUP BY pk.ID_POTRAVA;

-- Dotaz na vypísanie mien otcov a matiek zvierat, ktore ich maju definovane 
SELECT z.ID_ZVIERA as "Identifikacne cislo zvierata",z.Meno as "Meno zvierata",z.Druh as "Druh zvierata", COALESCE((SELECT Meno FROM zviera z WHERE z.ID_ZVIERA = r.ID_OTEC),Meno_otca) as Otec, COALESCE((SELECT Meno FROM zviera z WHERE z.ID_ZVIERA = r.ID_MATKA),Meno_matky) as Matka FROM zviera z
JOIN rodicia r ON r.ID_RODICOV = z.ID_RODICIA;


-- Dotaz na vypísanie zvierat, ktoré zahynuli/žijú v zoo
SELECT *
FROM zviera
WHERE zviera.stav =1;

-- Dotaz na vypísanie plánov kŕmenia s najväčším množstvom spotrebovanej potravy od nejakeho datumu…
SELECT pk.ID_PLAN_KRMENIA, CONCAT(k.Meno," ",k.Priezvisko) as "Meno krmica",p.Nazov as "Potrava",COUNT(sk.ID_SPOTREBA) as Pocet_krmeni, COUNT(DISTINCT(pz.ID_ZVIERA)) as Pocet_zvierat_v_plane, pk.Mnozstvo as "Mnozstvo na jedinca(g)" ,(COUNT(pz.ID_ZVIERA)*pk.Mnozstvo*COUNT(DISTINCT(pz.ID_ZVIERA))/1000) as Spotrebnovane_mnozstvo_kg FROM spotreba_krmiva sk
JOIN plan_krmenia pk ON pk.ID_PLAN_KRMENIA = sk.ID_PLAN_KRMENIA
JOIN plan_zvierata pz ON pz.ID_PLAN_KRMENIA = pk.ID_PLAN_KRMENIA
JOIN krmic k ON k.ID_KRMIC = pk.ID_KRMIC
JOIN potrava p ON p.ID_POTRAVA = pk.ID_POTRAVA
WHERE DATE(sk.Datum_cas) > '2017-01-01' AND DATE(sk.Datum_cas) < DATE(NOW())
GROUP BY sk.ID_PLAN_KRMENIA
ORDER BY Spotrebnovane_mnozstvo_kg DESC;

-- Dotaz na vypísanie “najnáročnejšieho plánu” kŕmenia podla množstva potravy/poctu zvierat
SELECT pk.ID_PLAN_KRMENIA, CONCAT(k.Meno," ",k.Priezvisko) as "Meno krmica",pk.Cas as "Cas krmenia",pk.Mnozstvo "Mnozstvo na jedinca",COUNT(pz.ID_PLAN_KRMENIA) as Pocet_jedincov_na_krmenie, SUM(pk.Mnozstvo) as Celkove_mnozstvo FROM plan_krmenia pk
JOIN plan_zvierata pz ON pz.ID_PLAN_KRMENIA = pk.ID_PLAN_KRMENIA
JOIN krmic k ON k.ID_KRMIC = pk.ID_KRMIC
JOIN potrava p ON p.ID_POTRAVA = pk.ID_POTRAVA
GROUP BY pz.ID_PLAN_KRMENIA
ORDER BY Celkove_mnozstvo DESC;

-- Dotaz na vypísanie všetkých dodávateľov
SELECT * FROM dodavatel d
JOIN adresa a ON a.ID_ADRESA = d.ID_ADRESA;

-- Dotaz na vypísanie všetkých dodávateľov z konkrétnej krajiny …
SELECT *
FROM dodavatel a
JOIN adresa b ON a.ID_ADRESA = b.ID_ADRESA
WHERE b.Krajina = "USA";

-- Dotaz na vypísanie všetkých druhov potráv podľa druhu uskladnenia
SELECT *
FROM potrava
ORDER BY Uskladnenie;

-- Dotaz na vypísanie kŕmiča s najväčšími skúsenosťami (podľa odpracovaných dni v zoo)
SELECT CONCAT(Meno," ",Priezvisko) AS "Meno a priezvisko", DATEDIFF(NOW(),Datum_nastupu) AS skusenosti
FROM krmic 
ORDER BY skusenosti DESC 
LIMIT 0,1;

-- Dotaz na sumu potrebnu na vyplatenie vsetkych krmicov pre aktualny mesiac
SELECT SUM(suma) as "Suma potrebna na vyplatu pre aktualny mesiac" 
FROM 
(SELECT (SUM(TIME_TO_SEC(TIMEDIFF(sk.Koniec,sk.Datum_cas)))/3600*k.Plat) as suma FROM spotreba_krmiva sk
JOIN plan_krmenia pk ON pk.ID_PLAN_KRMENIA = sk.ID_PLAN_KRMENIA
JOIN krmic k ON k.ID_KRMIC = pk.ID_KRMIC
WHERE YEAR(sk.Datum_cas) = YEAR(NOW()) AND MONTH(sk.Datum_cas) = MONTH(NOW())
GROUP BY pk.ID_KRMIC) as tabulka;


-- Dotaz na štatisticky údaj o tom z ktorých miest pochádza najviac zamestnancov
SELECT a.Mesto AS Mesto, count( * ) AS Pocet
FROM adresa a
JOIN krmic b ON a.ID_ADRESA = b.ID_ADRESA
GROUP BY a.Mesto
ORDER BY a.Mesto DESC;

-- Dotaz na množstvo potravín uskladnených v zoo a ich pôvod
SELECT a.Krajina as Krajina, sum(c.Zasoby) as "Dodane mnozstvo"  FROM adresa a join dodavatel b on a.ID_ADRESA=b.ID_ADRESA join potrava c on b.ID_DODAVATEL=c.ID_DODAVATEL group by b.ID_ADRESA order by "Uskladnene mnozstvo" desc;

-- Dotaz na záznam o skonzumovanom množstve potravy zvieraťom
SELECT z.Druh, p.Nazov,COUNT(pk.ID_PLAN_KRMENIA) as Pocet_krmeni, pk.Mnozstvo*COUNT(pk.ID_PLAN_KRMENIA) as "Spotrebovane(g)" FROM zviera z
JOIN plan_zvierata pz ON pz.ID_ZVIERA = z.ID_ZVIERA
JOIN plan_krmenia pk ON pk.ID_PLAN_KRMENIA = pz.ID_PLAN_KRMENIA
JOIN potrava p ON p.ID_POTRAVA = pk.ID_POTRAVA
JOIN spotreba_krmiva sp ON sp.ID_PLAN_KRMENIA = pz.ID_PLAN_KRMENIA
WHERE z.Meno = "Pokabal"
GROUP BY p.Nazov;

-- Dotaz na štatistický údaj o počte narodených/počet zvierat ktoré niesu zive
SELECT COUNT(*) from zviera where zviera.Stav=0;

-- Dotaz na počet zahraničných zamestnancov
SELECT count(*) as "Pocet zamestnanacov zo zahranicia" from krmic a 
JOIN adresa b on a.ID_ADRESA=b.ID_ADRESA 
WHERE b.Krajina not in("Slovenská republika");
-- Dotaz na krmičov ktorý majú/nemajú titul
SELECT * from krmic where krmic.Titul = NULL;

-- Dotaz na počet samcov a samičiek podľa jednotlivých druhov zvierat v zoo 
SELECT Druh,Pohlavie,COUNT(*) as "Pocet jedincov" FROM `zviera` 
WHERE 1
GROUP BY Pohlavie, Druh
ORDER BY Druh;

-- Dotaz pre zistenie aku potravu vyzaduju druhy zvierat pre potrebnu vytvarania planu krmenia

SELECT z.Druh as "Druh zvierata", p.Nazov as "Potrava pre dany druh" FROM zviera_potrava zp
JOIN zviera z ON z.ID_ZVIERA = zp.ID_ZVIERA
JOIN potrava p ON p.ID_POTRAVA = zp.ID_POTRAVA
GROUP BY zp.ID_POTRAVA,z.Druh;
