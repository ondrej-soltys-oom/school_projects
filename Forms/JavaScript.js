function validation(event) {


    var age = document.getElementById("vek").value;
    var birthday = new Date(document.getElementById("datum").value);
    var today = new Date();


    //Age validation
    if (document.getElementById("vek").value && !isNaN(birthday.getDay())) {
        if (((today.getFullYear() - 1) - birthday.getFullYear()) == age && today.getMonth() < birthday.getMonth()) {
            valAge = true;
        }
        else if ((today.getFullYear() - birthday.getFullYear()) == age) {
            valAge = true;
        }
        else if (((today.getFullYear() - 1) - birthday.getFullYear()) == age && today.getMonth() == birthday.getMonth() && today.getDate() >= birthday.getDate()) {
            valAge = true;
        }
        else {

            alert("zly vek");
            event.preventDefault();
            return valAge = false;
        }
    }


    var mail = document.getElementById("posta");
    if (mail.validity.patternMismatch) {
        window.alert("Zly format pre email");
        event.preventDefault();
        return valEmail = false;
    }


    var name = document.getElementById("name");
    if (name.validity.patternMismatch) {
        window.alert("Zly format pre meno");
        event.preventDefault();
        return valName = false;
    }


    var name = document.getElementById("surname");
    if (name.validity.patternMismatch) {
        window.alert("Zly format pre meno");
        event.preventDefault();
        return valSur = false;
    }

    return true;

}


function moje() {


    var radiobuttonOff = document.getElementById('radioOff').checked;
    var radiobuttonOn = document.getElementById('radioOn').checked;
    if (radiobuttonOff == true) {
        document.getElementsByClassName('MiestoKupa')[0].style.visibility = 'visible';
        document.getElementsByClassName('MiestoKupa')[0].style.height = 'auto';
        document.getElementsByClassName('MiestoKupa')[0].style.padding = '5.6px 12px 10px 12px';
    }
    if (radiobuttonOn == true) {
        document.getElementsByClassName('MiestoKupa')[0].style.visibility = 'hidden';
        document.getElementsByClassName('MiestoKupa')[0].style.height = '0px';
        document.getElementsByClassName('MiestoKupa')[0].style.padding = '0px';
    }
}

function znal() {

    if (document.getElementById('ine').checked) {
        document.getElementById('textIne').style.visibility = 'visible';
    }
    else {
        document.getElementById('textIne').style.visibility = 'hidden';
    }

}


function zmen() {

    var vybranyKraj = document.getElementById('okres');
    var vypisMesta = document.getElementById("mesta");
    var vypisUlic = document.getElementById("ulice");

    var zoznamMiest = [];
    zoznamMiest["empty"] = ["..."];
    zoznamMiest["BA"] = ["...", "Bratislava"];
    zoznamMiest["KE"] = ["...", "Kosice", "Trebisov"];
    //zoznamFakult["empty"].forEach(function(item){ vypisMesta.options[vypisMesta.options.length]= new Option(item); });

    vybranyKraj.onchange = function () {
        var thisArr = zoznamMiest[vybranyKraj.value];
        console.log(thisArr);
        vypisMesta.options.length = 0;
        thisArr.forEach(function (item) {
            vypisMesta.options[vypisMesta.options.length] = new Option(item);

        });
        vypisUlic.options.length = 0;
    };

    var vypisMesta2 = document.getElementById("mesta");
    var zoznamUlic = [];
    zoznamUlic["empty2"] = ["..."];
    zoznamUlic["Bratislava"] = ["Mojzesová 32", "Vajnorská 45", "Zochová 5"];
    zoznamUlic["Kosice"] = ["Hlavná 54", "Navarová 54"];
    zoznamUlic["Trebisov"] = ["Kolarová 4"];
    //zoznamUlic["empty2"].forEach(function(item){ vypisMesta.options[vypisMesta.options.length]= new Option(item); });

    vypisMesta2.onchange = function () {
        var thisArr2 = zoznamUlic[vypisMesta2.value];
        console.log(thisArr2);
        vypisUlic.options.length = 0;
        thisArr2.forEach(function (item) {
            vypisUlic.options[vypisUlic.options.length] = new Option(item);
        });
    };
}

zmen();


function Age() {
    var age = document.getElementById("vek").value;
    var birthday = new Date(document.getElementById("datum").value);
    var today = new Date();


    //Age validation
    if (document.getElementById("vek").value && !isNaN(birthday.getDay())) {
        if ((((today.getFullYear() - 1) - birthday.getFullYear()) == age && today.getMonth() <= birthday.getMonth() && today.getDay() >= birthday.getDay()) ||
            (((today.getFullYear() - birthday.getFullYear()) == age) && today.getMonth() >= birthday.getMonth() && today.getDay() >= birthday.getDay())) {
            valAge = true;
        }
        else {

            alert("zly vek");
            valAge = false;
        }
    }

}

function checkMail() {
    var mail = document.getElementById("posta");
    if (mail.validity.patternMismatch) {
        window.alert("Zly format pre email");
        return valEmail = false;
    } else {
        return valEmail = true;
    }
}

function checkName() {
    var name = document.getElementById("name");
    if (name.validity.patternMismatch) {
        window.alert("Zly format pre meno");
        return valName = false;
    } else {
        return valName = true;
    }
}

function checkSur() {
    var name = document.getElementById("surname");
    if (name.validity.patternMismatch) {
        window.alert("Zly format pre meno");
        return valSur = false;
    } else {
        return valSur = true;
    }
}

function trySubmit() {
    if (valAge && valEmail && valName && valSur) {
        return document.getElementById("odoslat").disabled = false;
    }
    else {
        return document.getElementsByName("odoslat").disabled = true;
    }
}

