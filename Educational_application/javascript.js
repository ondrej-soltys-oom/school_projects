var gameInfo = {
    pocetHier: 0,
    totalScore: 0,
    scoreGame: 0,
    chyby: 0,
    spravne: 0,
    step: 1,
    hraneHry: 1,
    povoleneKrokspat: 3
};

$(document).ready(function(){
    // for(var i = 1; i<=10; i++) {
    //     var string = "xml/znacky"".xml"
    lastGameInfo = localStorage.getItem("gameInfo");
    console.log(lastGameInfo);
    if(lastGameInfo){
        gameInfo = JSON.parse(lastGameInfo);

        pipravHru(gameInfo.pocetHier, gameInfo.scoreGame, gameInfo.totalScore, gameInfo.chyby, gameInfo.spravne);
        getLocalStorageStep(gameInfo.step);
    }else{
        pipravHru();
    }


    setInterval(function() {
        var cTime = new Date();
        var hh = cTime.getHours();
        var mm = cTime.getMinutes();
        var ss = cTime.getSeconds();
        var y = cTime.getFullYear();
        var m = cTime.getMonth() + 1;
        var d = cTime.getDate();
        // document.getElementById(".clock").innerHTML = m + "/" + d + "/" + y;
        //Add zeros
        hh = (hh < 10 ? "0" : "") + hh;
        mm = (mm < 10 ? "0" : "") + mm;
        ss = (ss < 10 ? "0" : "") + ss;
        // Compose the string for display
        var cTimeString = hh + ":" + mm + ":" + ss +" " + m + "/" + d + "/" + y;
        $(".clock").append().html(cTimeString);
    }, 1000);
    $('#toggle_footer').click(function () {
        $(this).parent().find(".container").toggle();
        // totalScore = $('set_score').value();

    });
    $("#change_game_play_data").submit(function (e) {
        e.preventDefault();
        console.log('submit triggered');
        var level = $(this).find('#set_level').val();
        var score =  $(this).find('#set_score').val();
        gameInfo.hraneHry--;
        pipravHru(level, gameInfo.scoreGame, score, gameInfo.chyby, gameInfo.spravne);
        return false;
    });
    $('#spat').click(function () {
         if(gameInfo.povoleneKrokspat > 0 && $( ".draggable" ).length !==1){
             gameInfo.povoleneKrokspat--;
             gameInfo.scoreGame = gameInfo.scoreGame -100;
             $('#score-value').html(gameInfo.scoreGame);
            getLocalStorageStep(--gameInfo.step);
         }

    });

    $('#reset').click(function () {
        if(gameInfo.step !== 1) {
            gameInfo.povoleneKrokspat = 3;
            getLocalStorageStep(1);
        }

    });
    $('#ulozHru').click(function () {
        gameInfo.step++;
        //console.log(gameInfo);
        saveLocalStorage("gameInfo", JSON.stringify(gameInfo));
        saveLocalStorage(("step_" + gameInfo.step), $(".draggables").html());

    });
    $('#akoHrat').click(function () {
        $( "#hraSaToTakto" ).toggle();

    });


});
function saveStepAndMove(){


    saveLocalStorage(("step_" + gameInfo.step), $(".draggables").html());
    gameInfo.step++;

}
function parseXML(xmlParameter){
    var znacky = $(xmlParameter).find("img").map(function() {
        return this.innerHTML;
    }).get();

    var vyklad = $(xmlParameter).find("sk").map(function() {
        return this.innerHTML;
    }).get();

    // var i = (((pocetHier-1)*10)+1); i < ((pocetHier*10)+1); i++
    for (var i = 1; i < 11; i++) {
        var index = Math.floor(Math.random() * 10 + ((gameInfo.pocetHier-1)*10));
        if(index>=(((gameInfo.pocetHier-1)*10)+5)){
            $('<div/>', {
                id: "drag" + i,
                class: 'draggable ui-widget-content',

                html: '<p>' + vyklad[Math.floor(Math.random() * vyklad.length)] + '</p>', //Random description to be compared with the alt attribute of the image
            }).appendTo('.draggables')
                .prepend(
                    $('<img/>', {
                        alt: vyklad[index], //spravny popis znacky
                        src: znacky[index], //img zo znacky.xml
                    })
                );
        }
        else{
            $('<div/>', {
                id: "drag" + i,
                class: 'draggable ui-widget-content',

                html: '<p>' + vyklad[index] + '</p>', //Random description to be compared with the alt attribute of the image
            }).appendTo('.draggables')
                .prepend(
                    $('<img/>', {
                        alt: vyklad[index], //spravny popis znacky
                        src: znacky[index], //img zo znacky.xml
                    })
                );
        }

    }

}
function pipravHru(level, score, totalscore, mistake, right){

    $('#obrazky').empty();


    if(typeof level !== 'undefined'){

        gameInfo.pocetHier = parseInt(level);
    }else{
        gameInfo.pocetHier += 1;
    }
    if(typeof totalscore !== 'undefined'){
        $('#highScore-value').html(totalscore);
    }
    if(typeof totalscore !== 'undefined'){
        $('#score-value').html(score);
    }
    if(typeof mistake !== 'undefined'){
        $('#mistakes-value').html(mistake);
    }
    if(typeof right !== 'undefined'){
        $('#right-value').html(right);
    }
    if(gameInfo.pocetHier > 10){

        gameInfo.totalScore =  parseInt($('#highScore-value').html());

        if(gameInfo.scoreGame > gameInfo.totalScore) {
            gameInfo.totalScore = parseInt($('#score-value').html());
            $('#highScore-value').html(gameInfo.totalScore);
        }
        setTimeout(function()
        {

        }, 300);
        alert("Prešiel si hru");
        var win = document.getElementById("winout");
        win.play();
        gameInfo.pocetHier = 1;
        gameInfo.scoreGame = 0;
        $('#score-value').html(gameInfo.scoreGame);
        $('#right-value').html(gameInfo.scoreGame);
        $('#mistakes-value').html(gameInfo.scoreGame);
        gameInfo.hraneHry++;
    }
    $("#lvl").html("Level "+gameInfo.pocetHier);
    if(gameInfo.hraneHry===1){
        $.ajax({
            type: "GET",
            url: "xml/znacky0.xml",
            dataType: "xml",
            success: function (xml) {
                parseXML(xml);
                gameInfo.hraneHry++;
                game();
            }

        });
    }
}


 function game() {

    $( ".draggable" ).draggable(  {
        revert: "invalid",
        containment : "#static",

    });



        $("#spravne").droppable({

            drop: function(event, ui) {
                saveStepAndMove();
                var ok = $(ui.draggable).find("img").attr("alt");
                var wrong = $(ui.draggable).find("p").text();
                if (ok != wrong) {
                    gameInfo.chyby+=1;
                    $('#mistakes-value').html(gameInfo.chyby);
                    var myObject = $(this);

                    myObject.addClass("dropped-wrong");
                    setTimeout(function()
                    {
                        myObject.removeClass("dropped-wrong");
                    }, 300);

                        var sad = document.getElementById("sad");
                        sad.play();
                    gameInfo.scoreGame = gameInfo.scoreGame -5;
                    $('#score-value').html(gameInfo.scoreGame);
                    // myObject.removeClass("dropped-wrong");

                } else {
                    gameInfo.spravne+=1;
                    $('#right-value').html(gameInfo.spravne);
                    var myObject = $(this);

                    myObject.addClass("dropped-ok");
                    setTimeout(function()
                    {
                        myObject.removeClass("dropped-ok");
                    }, 300);

                        var wau = document.getElementById("wau");
                        wau.play();
                    gameInfo.scoreGame = gameInfo.scoreGame +10;
                    $('#score-value').html(gameInfo.scoreGame);

                }


                $(ui.draggable).remove();
                if($( ".draggable" ).length === 0 ){
                    gameInfo.povoleneKrokspat =3;
                    gameInfo.step = 1;
                    gameInfo.hraneHry--;
                    pipravHru();
                }
            }

        });


        $("#nespravne").droppable({

            drop: function(event, ui) {
                saveStepAndMove();
                var ok = $(ui.draggable).find("img").attr("alt");
                var wrong = $(ui.draggable).find("p").text();

                if (ok === wrong) {
                    gameInfo.chyby+=1;
                    $('#mistakes-value').html(gameInfo.chyby);
                    var myObject = $(this);

                    myObject.addClass("dropped-wrong");
                    setTimeout(function()
                    {
                        myObject.removeClass("dropped-wrong");
                    }, 300);

                        var sad = document.getElementById("sad");
                        sad.play();
                    gameInfo.scoreGame = gameInfo.scoreGame -5;
                    $('#score-value').html(gameInfo.scoreGame);

                } else {
                    gameInfo.spravne+=1;
                    $('#right-value').html(gameInfo.spravne);

                    var myObject = $(this);
                    // clearBox('.draggables');

                    myObject.addClass("dropped-ok");
                    setTimeout(function()
                    {
                        myObject.removeClass("dropped-ok");
                    }, 300);

                        var wau = document.getElementById("wau");
                        wau.play();
                    gameInfo.scoreGame = gameInfo.scoreGame +10;
                    $('#score-value').html(gameInfo.scoreGame);

                }

                $(ui.draggable).remove();
                if($( ".draggable" ).length === 0 ){

                    // if(pocetHier ==10){
                    //         var winout = document.getElementById("winout");
                    //         winout.play();
                    //
                    // }
                    gameInfo.povoleneKrokspat =3;
                    gameInfo.step = 1;
                    gameInfo.hraneHry--;
                    pipravHru();
                }
                // console.log(wrong);
            }


        });


     // function clearBox(className)
     // {
     //     document.getElementsByClassName(className).innerHTML = "";
     // }
     // }


}
function saveLocalStorage(key, value) {
    if (typeof(Storage) !== "undefined") {
        //var pocetHier = 0;
        // var totalScore = 0;
        // var step = 1;
        // var povoleneKrokspat = 3;
        // Store
        localStorage.setItem(key, value);
        // Retrieve
        //console.log(localStorage.getItem(value));
    } else {
        // document.getElementById("result").innerHTML = "Sorry, your browser does not support Web Storage...";
    }
}
function getLocalStorageStep(value) {
    $('.draggables').empty();
    if(typeof value !== 'undefined'){
        gameInfo.step = value;
    }else{
        gameInfo.step;
    }

    var $html = $($.parseHTML(localStorage.getItem('step_' + (gameInfo.step))));
    $html.filter('.draggable').css({
       left: 0, top: 0
    });
    $('#obrazky').html($html);

    game();
}