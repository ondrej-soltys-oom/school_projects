<?php

$data = [
    'A' => 20,
    'B' => 11,
    'C' => 13,
    'D' => 7,
    'E' => 5,
    'FX' => 0,
    'FN' => 1,
    'Rok' => '2012/13',

];
$data2 = [
    'A' => 20,
    'B' => 19,
    'C' => 6,
    'D' => 3,
    'E' => 1,
    'FX' => 0,
    'FN' => 0,
    'Rok' => '2013/14',
];
$data3 = [
    'A' => 9,
    'B' => 19,
    'C' => 22,
    'D' => 0,
    'E' => 0,
    'FX' => 0,
    'FN' => 3,
    'Rok' => '2014/15',
];
$data4 = [
    'A' => 10,
    'B' => 33,
    'C' => 35,
    'D' => 20,
    'E' => 14,
    'FX' => 0,
    'FN' => 0,
    'Rok' => '2015/16',
];
$data5 = [
    'A' => 5,
    'B' => 18,
    'C' => 31,
    'D' => 26,
    'E' => 27,
    'FX' => 18,
    'FN' => 3,
    'Rok' => '2016/17',
];
function createPie($data){


    global $xBeginPosition, $image, $pieWidth, $pieHeight, $yBeginPosition, $font, $fontSize;
    $colorA = imagecolorallocate($image, 42, 98, 33);
    $colorB = imagecolorallocate($image, 61, 149, 183);
    $colorC = imagecolorallocate($image, 158, 53, 149);
    $colorD = imagecolorallocate($image, 177, 227, 229);
    $colorE = imagecolorallocate($image, 255, 255, 0);
    $colorFX = imagecolorallocate($image, 255, 0, 0);
    $colorFN = imagecolorallocate($image, 110, 198, 207);
    $colorPopis = imagecolorallocate($image, 0, 0, 0);
    $NumberOfStudents = 0;
    $index = 0;
    foreach ($data as $key => $value) {
        if($index != 7){
            $NumberOfStudents+=(int) $value;
        }
        $index++;
    }
    $perCoeficient = 100/$NumberOfStudents;
    $index = 0;
    $begin = 0;
    $end = 0;
    $coeficient = 360/$NumberOfStudents;
    $gridLegend = $xBeginPosition-$pieHeight+10;
    $level = $yBeginPosition + $pieHeight;
    imagettftext($image, 13, 0, $gridLegend+($pieWidth/3), $level, $colorPopis, $font, "Legenda:");
    $level += 20;
    foreach ($data as $key => $value) {

            if($index != 7){
                $begin = $end;
                $end = $end + (int)$value*$coeficient;
            }


                switch ($index) {
                    case 0:
                        if($value !=0){
                            imagefilledarc($image, $xBeginPosition, $yBeginPosition, $pieWidth, $pieHeight, $begin, $end, $colorA, IMG_ARC_PIE);
                        }
                        imagettftext($image, 13, 0, $gridLegend, $level, $colorA, $font, "A");
                        imagettftext($image, 13, 0, $gridLegend+($pieWidth/3), $level, $colorPopis, $font, $value);
                        imagettftext($image, 13, 0, $gridLegend+($pieWidth/3)+($pieWidth/3), $level, $colorPopis, $font, round(($perCoeficient*$value), 2) ."%") ;
                        $level += 20;
                        break;
                    case 1:
                        if($value !=0){
                            imagefilledarc($image, $xBeginPosition, $yBeginPosition, $pieWidth, $pieHeight, $begin, $end, $colorB, IMG_ARC_PIE);
                        }

                        imagettftext($image, 13, 0, $gridLegend, $level, $colorB, $font, "B");
                        imagettftext($image, 13, 0, $gridLegend+($pieWidth/3), $level, $colorPopis, $font, $value);
                        imagettftext($image, 13, 0, $gridLegend+($pieWidth/3)+($pieWidth/3), $level, $colorPopis, $font, round(($perCoeficient*$value), 2) ."%") ;
                        $level += 20;
                        break;
                    case 2:
                        if($value !=0){
                            imagefilledarc($image, $xBeginPosition, $yBeginPosition, $pieWidth, $pieHeight, $begin, $end, $colorC, IMG_ARC_PIE);
                        }

                        imagettftext($image, 13, 0, $gridLegend, $level, $colorC, $font, "C");
                        imagettftext($image, 13, 0, $gridLegend+($pieWidth/3), $level, $colorPopis, $font, $value);
                        imagettftext($image, 13, 0, $gridLegend+($pieWidth/3)+($pieWidth/3), $level, $colorPopis, $font, round(($perCoeficient*$value), 2) ."%") ;
                        $level += 20;
                        break;
                    case 3:
                        if($value !=0){
                            imagefilledarc($image, $xBeginPosition, $yBeginPosition, $pieWidth, $pieHeight, $begin, $end, $colorD, IMG_ARC_PIE);
                        }

                        imagettftext($image, 13, 0, $gridLegend, $level, $colorD, $font, "D");
                        imagettftext($image, 13, 0, $gridLegend+($pieWidth/3), $level, $colorPopis, $font, $value);
                        imagettftext($image, 13, 0, $gridLegend+($pieWidth/3)+($pieWidth/3), $level, $colorPopis, $font, round(($perCoeficient*$value), 2) ."%") ;
                        $level += 20;
                        break;
                    case 4:
                        if($value !=0){
                            imagefilledarc($image, $xBeginPosition, $yBeginPosition, $pieWidth, $pieHeight, $begin, $end, $colorE, IMG_ARC_PIE);
                        }

                        imagettftext($image, 13, 0, $gridLegend, $level, $colorE, $font, "E");
                        imagettftext($image, 13, 0, $gridLegend+($pieWidth/3), $level, $colorPopis, $font, $value);
                        imagettftext($image, 13, 0, $gridLegend+($pieWidth/3)+($pieWidth/3), $level, $colorPopis, $font, round(($perCoeficient*$value), 2) ."%") ;
                        $level += 20;
                        break;
                    case 5:
                        if($value !=0){
                            imagefilledarc($image, $xBeginPosition, $yBeginPosition, $pieWidth, $pieHeight, $begin, $end, $colorFX, IMG_ARC_PIE);
                        }

                        imagettftext($image, 13, 0, $gridLegend, $level, $colorFX, $font, "FX");
                        imagettftext($image, 13, 0, $gridLegend+($pieWidth/3), $level, $colorPopis, $font, $value);
                        imagettftext($image, 13, 0, $gridLegend+($pieWidth/3)+($pieWidth/3), $level, $colorPopis, $font, round(($perCoeficient*$value), 2) ."%") ;
                        $level += 20;
                        break;
                    case 6:
                        if($value !=0){
                            imagefilledarc($image, $xBeginPosition, $yBeginPosition, $pieWidth, $pieHeight, $begin, $end, $colorFN, IMG_ARC_PIE);
                        }

                        imagettftext($image, 13, 0, $gridLegend, $level, $colorFN, $font, "FN");
                        imagettftext($image, 13, 0, $gridLegend+($pieWidth/3), $level, $colorPopis, $font, $value);
                        imagettftext($image, 13, 0, $gridLegend+($pieWidth/3)+($pieWidth/3), $level, $colorPopis, $font, round(($perCoeficient*$value), 2) ."%") ;
                        $level += 20;
                        break;
                    case 7:
                        $xValue =$xBeginPosition-$pieHeight +10+ ($pieWidth/3);
                        $yValue= $yBeginPosition - $pieHeight/2-10;
                        imagettftext($image, 13, 0, $xValue, $yValue, $colorPopis, $font, $value);
                        break;
                }





        //imagefilledarc($image, $xBeginPosition, 150, $pieWidth, $pieHeight, $begin, $end, $navy, IMG_ARC_PIE);
        $index++;
    }









    $xBeginPosition+= $pieWidth*1.2;
}


$imageWidth = 1100;
$imageHeight = 500;
$image = imagecreate($imageWidth, $imageHeight);
$fontpath = realpath('./');
putenv('GDFONTPATH=' . $fontpath);
$font = "OpenSans-Regular.ttf";
$fontSize = 10;
$xBeginPosition = 100;
$yBeginPosition = 150;
$pieWidth = $imageWidth/6;
$pieHeight = 100;

$white = imagecolorallocate($image, 0xFF, 0xFF, 0xFF);
$colorA = imagecolorallocate($image, 42, 98, 33);
$colorB = imagecolorallocate($image, 61, 149, 183);
$colorC = imagecolorallocate($image, 158, 53, 149);
$colorD = imagecolorallocate($image, 177, 227, 229);
$colorE = imagecolorallocate($image, 255, 255, 0);
$colorFX = imagecolorallocate($image, 255, 0, 0);
$colorFN = imagecolorallocate($image, 110, 198, 207);

createPie($data);
createPie($data2);
createPie($data3);
createPie($data4);
createPie($data5);


header('Content-type: image/png');
$path_image = "img/kolac.png";
imagepng($image, $path_image);

$width = imagesx( $image );
$height = imagesy( $image );


$thumbWidth = 300;
$new_width = $thumbWidth;
$new_height = floor( $height * ( $thumbWidth / $width ) );

$tmp_img = imagecreatetruecolor( $new_width, $new_height );

imagecopyresized( $tmp_img, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height );

imagepng($tmp_img, "thumbs/kolacThumb.png");
imagepng($image);

//imagejpeg( $tmp_img, $imgThumb );
imagedestroy($image);
?>