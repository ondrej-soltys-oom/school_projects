<!doctype html>
<html><head>
    <title>Grafy</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="styles.css">
    </head>
<body class="bg-light">

<div id="page" class="bg-light">
    <div class="container ">
        <div class="bg-info">
            <h1 class="text-center row-bottom-margin bg-info">PROSPECH NA FEI</h1>
            <div class="row">
                <div class="col-8"></div>

            </div>
        </div>
        <div class="menu bg-light">
            <div class="row">
                <div class="col-4 text-center">
                    <a href="stlpec.php"><img class="img-thumbnail btn-secondary" alt="stlpce" src="thumbs/stlpecThumb.png"></a>
                </div>
                <div class="col-4 text-center">
                    <a href="kolac.php"><img class="img-thumbnail btn-secondary" alt="kolac" src="thumbs/kolacThumb.png"></a>
                </div>
                <div class="col-4 text-center">
                    <a href="charts.html"><div class="btn btn-secondary center-block anim "><br><br> Google Charts</div></a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>


</body></html>