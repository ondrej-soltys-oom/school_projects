<?php
$img="savedexample.jpg";
$img2="kolac.jpg";
$imgThumb="chartThumb.jpg";
$imgThumb="kolacThumb.jpg";
$img = imagecreatefrompng($img);
$img2 = imagecreatefrompng($img2);

$width = imagesx( $img );
$height = imagesy( $img );
$thumbWidth = 100;
$new_width = $thumbWidth;
$new_height = floor( $height * ( $thumbWidth / $width ) );

$tmp_img = imagecreatetruecolor( $new_width, $new_height );

imagecopyresized( $tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height );

imagepgn( $tmp_img, $imgThumb );

$width = imagesx( $img2 );
$height = imagesy( $img2 );
$thumbWidth = 100;
$new_width = $thumbWidth;
$new_height = floor( $height * ( $thumbWidth / $width ) );

$tmp_img = imagecreatetruecolor( $new_width, $new_height );

imagecopyresized( $tmp_img, $img2, 0, 0, 0, 0, $new_width, $new_height, $width, $height );

imagejpeg( $tmp_img, $imgThumb );

?>