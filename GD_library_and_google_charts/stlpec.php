<?php

function vykresli($data)
{
    global $itemX, $barWidth, $gridBottom, $yMaxValue, $gridHeight, $chart, $fontSize, $font, $labelMargin, $barSpacing;
    $colorA = imagecolorallocate($chart, 42, 98, 33);
    $colorB = imagecolorallocate($chart, 61, 149, 183);
    $colorC = imagecolorallocate($chart, 158, 53, 149);
    $colorD = imagecolorallocate($chart, 177, 227, 229);
    $colorE = imagecolorallocate($chart, 255, 255, 0);
    $colorFX = imagecolorallocate($chart, 255, 0, 0);
    $colorFN = imagecolorallocate($chart, 110, 198, 207);
    $colorPopis = imagecolorallocate($chart, 0, 0, 0);
    $index = 0;
    foreach ($data as $key => $value) {
        // Draw the bar
        $labelBox = imagettfbbox($fontSize, 0, $font, $key);
        $labelWidth = $labelBox[4] - $labelBox[0];

        $labelX = $itemX - $labelWidth / 2;
        $labelY = $gridBottom + $labelMargin + $fontSize;
        if($index !=7){
            $x1 = $itemX - $barWidth / 2;
            $y1 = $gridBottom - $value / $yMaxValue * $gridHeight;
            $x2 = $itemX + $barWidth / 2;
            $y2 = $gridBottom - 1;
            $xText = $x1;
            $yText = $y1+($value / $yMaxValue)/2 * $gridHeight;}
        switch ($index) {
            case 0:
                imagefilledrectangle($chart, $x1, $y1, $x2, $y2, $colorA);
                //imagettftext($chart, $x1, $y1, ($x2-$x1)/2, ($y2-$y1)/2, $colorA, $font, $value);
                imagettftext($chart, 13, 0, $xText, $yText, $colorPopis, $font, $value);
                break;
            case 1:
                imagefilledrectangle($chart, $x1, $y1, $x2, $y2, $colorB);
                imagettftext($chart, 13, 0, $xText, $yText, $colorPopis, $font, $value);
                break;
            case 2:
                imagefilledrectangle($chart, $x1, $y1, $x2, $y2, $colorC);
                imagettftext($chart, 13, 0, $xText, $yText, $colorPopis, $font, $value);
                break;
            case 3:
                imagefilledrectangle($chart, $x1, $y1, $x2, $y2, $colorD);
                imagettftext($chart, 13, 0, $xText, $yText, $colorPopis, $font, $value);
                break;
            case 4:
                imagefilledrectangle($chart, $x1, $y1, $x2, $y2, $colorE);
                imagettftext($chart, 13, 0, $xText, $yText, $colorPopis, $font, $value);
                break;
            case 5:
                imagefilledrectangle($chart, $x1, $y1, $x2, $y2, $colorFX);
                imagettftext($chart, 13, 0, $xText, $yText, $colorPopis, $font, $value);
                break;
            case 6:
                imagefilledrectangle($chart, $x1, $y1, $x2, $y2, $colorFN);
                imagettftext($chart, 13, 0, $xText, $yText, $colorPopis, $font, $value);
                break;
            case 7:
                $xValue = $labelX - ($barSpacing*5);
                $yValue = $labelY +$fontSize*2;
                imagettftext($chart, $fontSize, 0, $xValue, $yValue, $colorPopis, $font, $value);
                break;
        }
        if($index !=7){
            imagettftext($chart, $fontSize, 0, $labelX, $labelY, $colorPopis, $font, $key);
        }


        $itemX += $barSpacing;
        $index++;
    }
}

/*
 * Chart data
 */

$data = [
    'A' => 20,
    'B' => 11,
    'C' => 13,
    'D' => 7,
    'E' => 5,
    'FX' => 0,
    'FN' => 1,
    'Rok' => '2012/13',

];
$data2 = [
    'A' => 20,
    'B' => 19,
    'C' => 6,
    'D' => 3,
    'E' => 1,
    'FX' => 0,
    'FN' => 0,
    'Rok' => '2013/14',
];
$data3 = [
    'A' => 9,
    'B' => 19,
    'C' => 22,
    'D' => 0,
    'E' => 0,
    'FX' => 0,
    'FN' => 3,
    'Rok' => '2014/15',
];
$data4 = [
    'A' => 10,
    'B' => 33,
    'C' => 35,
    'D' => 20,
    'E' => 14,
    'FX' => 0,
    'FN' => 0,
    'Rok' => '2015/16',
];
$data5 = [
    'A' => 5,
    'B' => 18,
    'C' => 31,
    'D' => 26,
    'E' => 27,
    'FX' => 18,
    'FN' => 3,
    'Rok' => '2016/17',
];

/*
 * Chart settings and create image
 */

// Image dimensions
$imageWidth = 1100;
$imageHeight = 500;

// Grid dimensions and placement within image
$gridTop = 100;
$gridLeft = 100;
$gridBottom = 450;
$gridRight = 950;
$gridHeight = $gridBottom - $gridTop;
$gridWidth = $gridRight - $gridLeft;
$level = $gridTop;
$gridLegend = ($imageWidth -$gridRight)/2 + $gridRight;
// Bar and line width
$lineWidth = 1;
$barWidth = 20;

// Font settings
$fontpath = realpath('./');
putenv('GDFONTPATH=' . $fontpath);
$font = "OpenSans-Regular.ttf";
//$font = 'OpenSans-Regular.ttf';
$fontSize = 10;

// Margin between label and axis
$labelMargin = 8;

// Max value on y-axis
$yMaxValue = 50;

// Distance between grid lines on y-axis
$yLabelSpan = 10;

// Init image
$chart = imagecreate($imageWidth, $imageHeight);

// Setup colors
$backgroundColor = imagecolorallocate($chart, 255, 255, 255);
$axisColor = imagecolorallocate($chart, 85, 85, 85);
$labelColor = $axisColor;
$colorA = imagecolorallocate($chart, 42, 98, 33);
$colorB = imagecolorallocate($chart, 61, 149, 183);
$colorC = imagecolorallocate($chart, 158, 53, 149);
$colorD = imagecolorallocate($chart, 177, 227, 229);
$colorE = imagecolorallocate($chart, 255, 255, 0);
$colorFX = imagecolorallocate($chart, 255, 0, 0);
$colorFN = imagecolorallocate($chart, 110, 198, 207);
$gridColor = imagecolorallocate($chart, 212, 212, 212);
$barColor = imagecolorallocate($chart, 0, 255, 0);
$barColor2 = imagecolorallocate($chart, 255, 0, 0);
$barColor3 = imagecolorallocate($chart, 0, 0, 255);
$barColor4 = imagecolorallocate($chart, 47, 133, 217);
$barColor5 = imagecolorallocate($chart, 0, 255, 0);


imagefill($chart, 0, 0, $backgroundColor);

imagesetthickness($chart, $lineWidth);

/*
 * Print grid lines bottom up
 */

for ($i = 0; $i <= $yMaxValue; $i += $yLabelSpan) {
    $y = $gridBottom - $i * $gridHeight / $yMaxValue;

    // draw the line
    imageline($chart, $gridLeft, $y, $gridRight, $y, $gridColor);

    // draw right aligned label
    $labelBox = imagettfbbox($fontSize, 0, $font, strval($i));
    $labelWidth = $labelBox[4] - $labelBox[0];

    $labelX = $gridLeft - $labelWidth - $labelMargin;
    $labelY = $y + $fontSize / 2;

    imagettftext($chart, $fontSize, 0, $labelX, $labelY, $labelColor, $font, strval($i));
}

/*
 * Draw x- and y-axis
 */

imageline($chart, $gridLeft, $gridTop, $gridLeft, $gridBottom, $axisColor);
imageline($chart, $gridLeft, $gridBottom, $gridRight, $gridBottom, $axisColor);
imagettftext($chart, 13, 90, 40, $imageHeight / 1.5, $labelColor, $font, "POČET ŠTUDENTOV");
imagettftext($chart, 13, 0, $imageWidth / 3, $gridTop / 2, $labelColor, $font, "Hodnotenie študentov");
imagettftext($chart, 13, 0, $gridLegend-30, $level, $labelColor, $font, "Legenda:");
$level += 20;
imagettftext($chart, 13, 0, $gridLegend, $level, $colorA, $font, "A");
$level += 20;
imagettftext($chart, 13, 0, $gridLegend, $level, $colorB, $font, "B");
$level += 20;
imagettftext($chart, 13, 0, $gridLegend, $level, $colorC, $font, "C");
$level += 20;
imagettftext($chart, 13, 0, $gridLegend, $level, $colorD, $font, "D");
$level += 20;
imagettftext($chart, 13, 0, $gridLegend, $level, $colorE, $font, "E");
$level += 20;
imagettftext($chart, 13, 0, $gridLegend, $level, $colorFX, $font, "FX");
$level += 20;
imagettftext($chart, 13, 0, $gridLegend, $level, $colorFN, $font, "FN");


/*
 * Draw the bars with labels
 */

$barSpacing = $gridWidth / (count($data) * 5);
$itemX = $gridLeft + $barSpacing / 2;

vykresli($data);
vykresli($data2);
vykresli($data3);
vykresli($data4);
vykresli($data5);
/*
 * Output image to browser
 */

header('Content-Type: image/png');
//imagepng($chart, "thumbs/chart.pgn");
//$png_image = imagecreate(150, 150);

$path_image = "img/savedexample.png";
imagepng($chart, $path_image);

$width = imagesx( $chart );
$height = imagesy( $chart );


$thumbWidth = 300;
$new_width = $thumbWidth;
$new_height = floor( $height * ( $thumbWidth / $width ) );

$tmp_img = imagecreatetruecolor( $new_width, $new_height );

imagecopyresized( $tmp_img, $chart, 0, 0, 0, 0, $new_width, $new_height, $width, $height );

imagepng($tmp_img, "thumbs/stlpecThumb.png");

imagepng($chart);
imagedestroy($chart);
?>