$(document).ready(function(){
    $("#posliDatum").click(function(){
        var datum = $("#datum").val();
        $.ajax({
            type: 'GET',
            url: 'http://147.175.98.235/Zadanie6/rest.php/datum/'+datum+'',
            success: function(msg){
                $("#vysledok").html(msg);
            }});
    });

    $("#sviatkySK").click(function(){
        $.ajax({
            type: 'GET',
            url: 'http://147.175.98.235/Zadanie6/rest.php/sviatky/SK',
            success: function(msg){
                $("#vysledok").html(msg);
            }
        });
    });

    $("#sviatkyCZ").click(function(){
        $.ajax({
            type: 'GET',
            url: 'http://147.175.98.235/Zadanie6/rest.php/sviatky/CZ',
            success: function(msg){
                $("#vysledok").html(msg);
            }
        });
    });

    $("#pamatnedniSK").click(function(){
        $.ajax({
            type: 'GET',
            url: 'http://147.175.98.235/Zadanie6/rest.php/pamatnedni',
            success: function(msg){
                $("#vysledok").html(msg);
            }
        });
    });
    $("#posliMenoStat").click(function(){
        var menoastat = $("#menoastat").val();
        menoastat = menoastat.split(",");
        $.ajax({
            type: 'GET',
            url: 'http://147.175.98.235/Zadanie6/rest.php/meno/'+menoastat[0]+'/stat/'+menoastat[1]+'',
            success: function(msg){
                $("#vysledok").html(msg);
            }
        });
    });
    $("#posliMenoDen").click(function(){
        var menomesiacden = $("#menomesiacden").val();
        menomesiacden = menomesiacden.split(",");
        $.ajax({
            type: 'PUT',
            url: 'http://147.175.98.235/Zadanie6/rest.php/meno/'+menomesiacden[0]+'/datum/'+menomesiacden[1]+'',
            success: function(msg){
                $("#vysledok").html(msg);
            }
        });
    });
    $("#nameaAndCountry").click(function(){
        var nameaCountry = $("#nameaCountry").val();
        nameaCountry = nameaCountry.split(",");
        $.ajax({
            type: 'POST',
            url: 'http://147.175.98.235/index.php',
            success: function(msg){
                $("#vysledok").html(msg);
            }
        });
    });
});