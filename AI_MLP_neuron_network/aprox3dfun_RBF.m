echo on
% Zadanie Neuronove siete - uloha 3
% Aproximacia 3D funkcie y=f(x1,x2) RBF sietou
clc
clear

% nacitanie dat
% x1, x2 - vstupne premenne funkcie, 3D zobrazenie (mesh, surf)
% y - vystupna premenna  funkcie, 3D zobrazenie (mesh, surf)

% x1r, x2r - vstupne premenne  funkcie pre trenovanie siete, zobrazenie (plot3)
% yr - vystupna premenna  funkcie pre trenovanie siete, zobrazenie (plot3)

% x1t, x2t - vstupne premenne  funkcie pre testovanie siete, zobrazenie (plot3)
% yt - vystupna premenna  funkcie pre testovanie siete, zobrazenie (plot3)

load data3dfun;

% vstupne data siete
xr=[x1r; x2r];
xt=[x1t; x2t];

% vytvorenie struktury RBF siete, aj jej trenovanie
net = newrb(xr,yr,0.0001);

% simulacia vystupu siete pre trenovacie data
z=sim(net,xr);

% simulacia vystupu siete pre testovacie data
zt=sim(net,xt);

% vypocet odchyliek pre trenovacie data
% absolutne odchylky
errtrain=abs(yr-z);

% sumy odchyliek
SSEtrain=sum((yr-z).^2)
MSEtrain=SSEtrain/length(yr)

% max. abs. odchylka
MAXABStrain=max(errtrain)

% vypocet odchyliek pre testovacie data
% absolutne odchylky
errtest=abs(yt-zt);

% sumy odchyliek
SSEtest=sum((yt-zt).^2)
MSEtest=SSEtest/length(yt)

% max. abs. odchylka
MAXABStest=max(errtest)

figure
plot3(x1r,x2r,yr,'b+',x1r,x2r,z,'r*')
title('trenovacie data')
xlabel('x1')
ylabel('x2')
zlabel('y')
legend('data','MLP')


figure
plot3(x1t,x2t,yt,'b+',x1t,x2t,zt,'r*')
title('testovacie data')
xlabel('x1')
ylabel('x2')
zlabel('y')
legend('data','MLP')

% vykreslenie tvaru 3D funkcie

% priprava dat pre siet
innet=[x1(:) x2(:)]';

% simulacia vystupu siete
outnet=net(innet);

% rozklad vystupu siete do matice pre 3D zobrazenie
yn=[];
nn=size(x1,1);
for i=1:size(x1,2),
    yn=[yn outnet(1+nn*(i-1):nn*i)'];
end

figure
surf(x1,x2,y)
title('original data')
xlabel('x1')
ylabel('x2')
zlabel('y')

figure
surf(x1,x2,yn)
title('vystup z RBF siete')
xlabel('x1')
ylabel('x2')
zlabel('y')

figure
surf(y-yn)
title('odchylky RBF siete od originalu')
xlabel('x1')
ylabel('x2')
zlabel('chyba')

echo off