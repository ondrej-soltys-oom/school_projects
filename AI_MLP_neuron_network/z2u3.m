clear
clc
load('data3dfun.mat');
close all

% urobi MLP siet
net = fitnet(15);

% vsetky data pouzite na trenovanie
net.divideFcn='divideind';  % podla indexov
net.divideParam.trainInd = 1:1:121;
%net.divideParam.valInd=0:0;  %validacia=overenie
net.divideParam.testInd = 122:1:562;

% Nastavenie parametrov trénovania
%net.trainParam.min_grad = 1e-6;
%net.trainParam.epochs = 10;
net.trainParam.goal = 1e-6;     % Ukoncovacia podmienka na chybu SSE.
net.trainParam.show = 5;      

X = [x1r x1t; x2r x2t];
Y = [yr, yt];

net = train(net, X, Y);
% vypocet odchyliek pre trenovacie data
% absolutne odchylky
tren = [x1r;x2r];
test = [x1t; x2t];

z=sim(net,tren);

% simulacia vystupu siete pre testovacie data
zt=sim(net,test);

errtrain=abs(yr-z);

% sumy odchyliek
SSEtrain = sum((yr-z).^2)
MSEtrain = SSEtrain/length(yr)

% max. abs. odchylka
MAXABStrain = max(errtrain)

% vypocet odchyliek pre testovacie data
% absolutne odchylky
errtest=abs(yt-zt);

% sumy odchyliek
SSEtest = sum((yt-zt).^2)
MSEtest = SSEtest/length(yt)

% max. abs. odchylka
MAXABStest = max(errtest)

figure(2)
title('vysledne data po douceni siete');
innet3=[x1(:) x2(:)]';
outnetsim3 = sim(net,innet3);
zsim3=reshape(outnetsim3,41,41);
mesh(x1,x2,zsim3)  %moje vysledky

figure(3)

mesh(x1,x2,abs(zsim3-y))  %odchylka
title('odchylka');

figure(1)

mesh(x1,x2,y); %demo
title('vstupne data');

figure(2)

innet3=[x1(:) x2(:)]';
outnetsim3 = sim(net,innet3);
zsim3=reshape(outnetsim3,41,41);
mesh(x1,x2,zsim3)  %moje vysledky
hold on 
title('porovnanie vystupov');
plot3(x1r,x2r,yr,'b+',x1t,x2t,yt,'r*')
hold off