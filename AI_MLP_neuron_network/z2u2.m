clear;
clc;

load datapiscisla_all;   

%prepare net
net = patternnet(50);

net.divideFcn='dividerand';
net.performFcn = 'mse';             % Sum-Squared Error performance function
net.trainParam.goal = 0.000001;     % Sum-squared error goal.
net.trainParam.show = 20;           % Frequency of progress displays (in epochs).
net.trainParam.epochs = 700;        % Maximum number of epochs to train.
net.trainParam.min_grad = 1e-10;    % ukoncovacia podmienka na min. gradient 

net.divideParam.trainRatio=0.6;
net.divideParam.valRatio=0.1;
net.divideParam.testRatio=0.3;

%dispznak(XDataall(:,1), 28, 28);

%prepare data 60%

% xdata(:, 1:2964) = XDataall(:, 1:2964);
% ydata(:, 1:2964) = YDataall(:, 1:2964);

%net = train(net,XDataall,YDataall);

net = train(net,XDataall,YDataall);
%y = net(X);
out_sim = sim(net, XDataall);
[a, b, c, d] = confusion(YDataall, out_sim);
dataYsim = sim(net,XDataall)
err=(dataYsim-YDataall);
figure 
surf(err)
title('Odchylka NS od trenovacich dat')

plotconfusion(YDataall, out_sim); 

% simulacia vystupu NS pre trenovacie data
% testovanie NS
outnetsim = sim(net,XDataall)

%test net
dataYsim = sim(net,XDataall)
err=(dataYsim-YDataall);
figure 
surf(err)
title('Odchylka NS od trenovacich dat')