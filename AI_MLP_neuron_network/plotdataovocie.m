clear
% jablko, hruska, banan, pomaranc, citron
load dataovocie

figure
plot3(data1(:,1),data1(:,2),data1(:,3),'b+')
hold on
plot3(data2(:,1),data2(:,2),data2(:,3),'co')
plot3(data3(:,1),data3(:,2),data3(:,3),'g*')
plot3(data4(:,1),data4(:,2),data4(:,3),'r*')
plot3(data5(:,1),data5(:,2),data5(:,3),'mx')
axis([0 1 0 1 0 1])
title('original data')
xlabel('pomer rozmerov')
ylabel('pomer obsahov')
zlabel('farba')

minima=[0.4 0.35 0.15];
maxima=[1.0 0.85 0.95];

data1n=[(data1(:,1)-minima(1))/(maxima(1)-minima(1)) (data1(:,2)-minima(2))/(maxima(2)-minima(2)) (data1(:,3)-minima(3))/(maxima(3)-minima(3))];
data2n=[(data2(:,1)-minima(1))/(maxima(1)-minima(1)) (data2(:,2)-minima(2))/(maxima(2)-minima(2)) (data2(:,3)-minima(3))/(maxima(3)-minima(3))];
data3n=[(data3(:,1)-minima(1))/(maxima(1)-minima(1)) (data3(:,2)-minima(2))/(maxima(2)-minima(2)) (data3(:,3)-minima(3))/(maxima(3)-minima(3))];
data4n=[(data4(:,1)-minima(1))/(maxima(1)-minima(1)) (data4(:,2)-minima(2))/(maxima(2)-minima(2)) (data4(:,3)-minima(3))/(maxima(3)-minima(3))];
data5n=[(data5(:,1)-minima(1))/(maxima(1)-minima(1)) (data5(:,2)-minima(2))/(maxima(2)-minima(2)) (data5(:,3)-minima(3))/(maxima(3)-minima(3))];

figure
plot3(data1n(:,1),data1n(:,2),data1n(:,3),'b+')
hold on
plot3(data2n(:,1),data2n(:,2),data2n(:,3),'co')
plot3(data3n(:,1),data3n(:,2),data3n(:,3),'g*')
plot3(data4n(:,1),data4n(:,2),data4n(:,3),'r*')
plot3(data5n(:,1),data5n(:,2),data5n(:,3),'mx')
axis([0 1 0 1 0 1])
title('normalizovane data')
xlabel('pomer rozmerov')
ylabel('pomer obsahov')
zlabel('farba')

