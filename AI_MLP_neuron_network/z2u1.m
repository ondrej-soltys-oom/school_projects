echo on

plotdataovocie;
close(1);
close(2);

net = patternnet(15);

% vsetky data pouzite na trenovanie
net.divideParam.trainRatio=0.6;
net.divideParam.valRatio=0;
net.divideParam.testRatio=0.4;

net.trainParam.goal = 0.000001;	    % Ukoncovacia podmienka na chybu SSE.
net.trainParam.epochs = 500;  	    % Max. pocet tr?novac?ch cyklov.
net.trainParam.min_grad = 1e-10;    % Ukoncovacia podmienka na min. gradient

X = [data1n ; data2n ; data3n ; data4n ; data5n ]';
P = [ones(1,20),zeros(1,80);
     zeros(1,20),ones(1,20),zeros(1,60);
     zeros(1,40),ones(1,20),zeros(1,40);
     zeros(1,60),ones(1,20),zeros(1,20);
     zeros(1,80),ones(1,20)
];

for i=1:10
    net = train(net,X,P);
    out_sim = sim(net, X);
    [a, b, c, d] = confusion(P ,out_sim);

    if a < 0.01
        plotconfusion(P, out_sim); 
        break;
    end
end

echo off