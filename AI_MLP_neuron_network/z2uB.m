
clc
clear
close all
Xr = zeros(615,11); %matica pre vstupne data 
Yt = zeros(615,29); %matica pre vystupne data 


delimiterIN = ' '; %oddeluje 
filename = 'input_data.txt'; 

[Xr,delimiterIN]=importdata(filename); %nacitanie suboru
x1= [Xr(:,1)];
y1= [Xr(:,2)];
x2= [Xr(:,3)];
y2 = [Xr(:,4)];
x3 = [Xr(:,5)];
y3 =[Xr(:,6)];
x4 =[Xr(:,7)];
y4 =[Xr(:,8)];
x5=[Xr(:,9)];
y5 =[Xr(:,10)];
p= [Xr(:,11)];

%delimiterIN = ' ';
filename2 = 'target_data.txt';

[Yt,delimiterIN]=importdata(filename2); %nacitanie suboru
x1t= [Yt(:,1)];
y1t= [Yt(:,2)];
x2t= [Yt(:,3)];
y2t = [Yt(:,4)];
x3t = [Yt(:,5)];
y3t =[Yt(:,6)];
x4t =[Yt(:,7)];
y4t =[Yt(:,8)];
x5t=[Yt(:,9)];
y5t =[Yt(:,10)];
pt= [Yt(:,11)];

%definicia dat pre nalsdovnu krizovu validaciu
Xr1 = [];
Yt1 = [];   
Xr2 = [];
Yt2 = [];    
Xr3 = [];
Yt3 = [];   
Xr4 = [];
Yt4 = [];    
Xr5 = [];
Yt5 = [];
%rozdelenie udajov vo for cykle
for i=1:615
    if(mod(i,5)==1)
    Xr1 = [Xr1;Xr(i,:)];
    Yt1 = [Yt1;Yt(i,:)];
    
    elseif(mod(i,5)==2)
    Xr2 = [Xr2;Xr(i,:)];
    Yt2 = [Yt2;Yt(i,:)];
    
    elseif(mod(i,5)==3)
    Xr3 = [Xr3;Xr(i,:)];
    Yt3 = [Yt3;Yt(i,:)]; 
    
    elseif(mod(i,5)==4)
    Xr4 = [Xr4;Xr(i,:)];
    Yt4 = [Yt4;Yt(i,:)];
    
    elseif(mod(i,5)==0)
    Xr5 = [Xr5;Xr(i,:)];
    Yt5 = [Yt5;Yt(i,:)];
    end
end
% definicia matic 
vstup = [];
vystup = [];
testVstup = [];
testVystup = [];
%jednovrstvova neuronova siet s 30 neuronmi 
net = patternnet(30);

% vsetky data pouzite na trenovanie 
net.divideFcn='divideint'; %kazda n-ta vzorka

net.divideParam.trainRatio=0.6;
net.divideParam.valRatio=0.1;
net.divideParam.testRatio=0.3;

net.trainParam.goal = 0.000001;	    % Ukoncovacia podmienka na chybu SSE.
net.trainParam.epochs = 200;  	    % Max. pocet tr?novac?ch cyklov.
net.trainParam.min_grad=1e-6;      % Ukoncovacia podmienka na min. gradient

for j=1:5
    switch(j)
        case 1
            vstup = [Xr1;Xr2;Xr3];
            vystup = [Yt1;Yt2;Yt3];
            testVstup = [Xr4;Xr5];
            testVystup = [Yt4;Yt5];
            
        case 2
            vstup = [Xr2;Xr3;Xr4];
            vystup = [Yt2;Yt3;Yt4];
            testVstup = [Xr1;Xr5];
            testVystup = [Yt1;Yt5];
            
        case 3
            vstup = [Xr3;Xr4;Xr5];
            vystup = [Yt3;Yt4;Yt5];
            testVstup = [Xr1;Xr2];
            testVystup = [Yt1;Yt2];
            
        case 4
            vstup = [Xr4;Xr5;Xr1];
            vystup = [Yt4;Yt5;Yt1];
            testVstup = [Xr2;Xr3];
            testVystup = [Yt2;Yt3];
            
        case 5
            vstup = [Xr5;Xr1;Xr2];
            vystup = [Yt5;Yt1;Yt2];
            testVstup = [Xr3;Xr4];
            testVystup = [Yt3;Yt4];       
    end
    
    [net, tr] = train(net,vstup',vystup');

    outSim = sim(net, testVstup');

    [a,b,c,d] = confusion(testVystup' ,outSim);

    if(a < 0.1)
        figure(1)
        plotconfusion(testVystup',outSim);

        figure(2)
        plotperform(tr);
        break;
    end
end

% simulacia vystupu siete pre trenovacie data
z=sim(net,Xr');

% vypocet odchyliek pre trenovacie data
% absolutne odchylky
errtrain=abs(Yt'-z);

% sumy odchyliek
SSEtrain=sum(sum((Yt'-z).^2))
MSEtrain=sum(SSEtrain/length(Yt'))

% max. abs. odchylka
MAXABStrain=sum(max(errtrain))

figure(3)
plotconfusion(Yt',z);
