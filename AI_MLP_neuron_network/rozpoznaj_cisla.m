echo on
% Program na natrenovanie NS na rozpoznavanie cislic 0 az 9
% NS je trojvrstvova siet s aktivacnymi funkciami 'logsig'
% NS ma 28 vstupov - raster 4x7 - hodnota 0 az 1
% NS ma 10 vystupov - ake cislo je rozpoznane - hodnota 0 az 1

% vygenerovanie dat pre cislice 0 az 9
[dataX,dataY]=gennumbers(0:9);
dispznak(dataX,7,4);

% zistenie rozmerov dat
[R,Q] = size(dataX);
[S2,Q] = size(dataY);

% vytvorenie struktury NS 
% 28 vstupov z rozsahom (0,1)
% 1 skryta vrstva s poctom neuronov 12 a funkciou 'logsig'
% 10 vystupov s funkciou 'logsig'
% trenovacia metoda - backpropagation s Momentum
S1 = 12;
% net = newff(minmax(dataX),[S1 S2],{'logsig' 'logsig'},'traingdx');
% % inicialiacia vah siete
% net.LW{2,1} = net.LW{2,1}*0.01;
% net.b{2} = net.b{2}*0.01;

net = patternnet(S1);

% vsetky data pouzite na trenovanie
net.divideFcn='dividetrain';

% nastavenie parametrov trenovania epoch
net.performFcn = 'mse';             % Sum-Squared Error performance function
net.trainParam.goal = 0.000001; % Sum-squared error goal.
net.trainParam.show = 20;           % Frequency of progress displays (in epochs).
net.trainParam.epochs = 500;       % Maximum number of epochs to train.
net.trainParam.min_grad = 1e-10;  % ukoncovacia podmienka na min. gradient 

% trenovanie NS
net = train(net,dataX,dataY);

% simulacia vystupu NS pre trenovacie data
% testovanie NS
outnetsim = sim(net,dataX)

% chyba NS a dat
err=(outnetsim-dataY);
figure 
surf(err)
title('Odchylka NS od trenovacich dat')

%------------------------------------------ Testovanie cisel 

% test 1
% cislo 2 so sumom 0.4			% noise interval <0; 0.5>
dataX=gennumbers(2,0.4);
dispznak(dataX,7,4);
cislo = sim(net,dataX)
cislo=round(cislo)'

% test 2
dataX=gennumbers(7,0.2);
dispznak(dataX,7,4);
cislo = sim(net,dataX)
cislo=round(cislo)'

echo off



