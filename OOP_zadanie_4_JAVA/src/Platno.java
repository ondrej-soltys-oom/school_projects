
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * @author Ondrej Soltys,
 * ID 80108
 * Zadanie Zadanie c.4
 */
public class Platno extends Canvas implements MouseListener, MouseMotionListener {

    private ArrayList<GeomTvar> poleGeometrickychTvarov;
    protected int index = 0;
    protected int indexPridaOdstran = 1;
    protected int farR = 0;
    protected int farG = 0;
    protected int farB = 255;
    private GeomTvar zmenobjekt;
    private double a;                                                          //premenne pre vypocet pozicie objektu pri zmene
    private double b;
    private int indexObjekt = -1;

    /**
     *
     * @param vstup nazov vstupneho suboru konstruktor vytvory arraylist a
     * zavola metodu zoSuboruDoArrayList a napni nam poleGeometrickychTvarov
     */
    public Platno(String vstup) {
        poleGeometrickychTvarov = new ArrayList<GeomTvar>();
        zoSuboruDoArrayList(vstup);
        this.sumGeomTvar();
        this.pocetGeomTvar();
        addMouseListener(this);
        addMouseMotionListener(this);

    }

    /**
     *
     * @return vrati nam poleGeometrickychTvarov kde su udaje o geometrickom
     * tvare
     */
    public ArrayList<GeomTvar> getPoleGeometrickychTvarov() {
        return poleGeometrickychTvarov;
    }

    /**
     *
     * @param pole vstupnz parameter typu ArrayList nastavy ArrayList
     *
     */
    public void setPoleGeometrickychTvarov(ArrayList<GeomTvar> pole) {
        this.poleGeometrickychTvarov = pole;
    }

    /**
     *
     * @param g grafika zdedena z triedy Canvas metoda vykresluje jednotlive
     * geometricke tvary
     */
    @Override
    public void paint(Graphics g) {

        for (GeomTvar it : this.getPoleGeometrickychTvarov()) {
            Color myColor = new Color(it.colR, it.colG, it.colB);
            switch (it.typ) {
                case 0:
                    g.setColor(myColor);
                    int priemer = 2 * ((int) ((Kruh) it).getPolomer());
                    g.fillOval(it.surX, it.surY, priemer, priemer);

                    break;

                case 1:
                    g.setColor(myColor);
                    g.fillRect(it.surX, it.surY, (int) ((Obdlznik) it).getSirka(), (int) ((Obdlznik) it).getVyska());

                    break;
                case 2:
                    g.setColor(myColor);
                    int vyska = (int) (Math.cos(Math.toRadians(90) - Math.toRadians(((Trojuholnik) it).getUholY())) * ((Trojuholnik) it).getB());
                    int x_a = (int) (Math.sin(Math.toRadians(90) - Math.toRadians(((Trojuholnik) it).getUholY())) * ((Trojuholnik) it).getB());
                    g.fillPolygon(new int[]{it.surX, (it.surX + (int) ((Trojuholnik) it).getA()), (it.surX + x_a)}, new int[]{it.surY, it.surY, (it.surY + vyska)}, 3);

                    break;
            }

        }
    }

    /**
     * spocita obvod a obsah jednotlivych objektov ulozenych v ArrayListe, ktore
     * nasledne aj vypise.
     */
    private void sumGeomTvar() {
        double sumObvod = 0;
        double sumObsah = 0;
        int i;
        for (i = 0; i < poleGeometrickychTvarov.size(); i++) {
            if (poleGeometrickychTvarov.get(i) != null) {
                sumObvod = sumObvod + this.poleGeometrickychTvarov.get(i).obvod;
                sumObsah = sumObsah + this.poleGeometrickychTvarov.get(i).obsah;
            }
        }
        System.out.println("suma obvodu objektov: " + sumObvod);
        System.out.println("suma obsahu objektov: " + sumObsah);
    }

    /**
     *
     * @param pole vstupom je pole ktore je naplnene udajmi o jednom
     * geometrickom utvare pridava jednotlive geometricke tvary do ArrayListu.
     * Zaroven osetruje korektny vstup lebo udaje, ktore dostane zo subora na
     * dany geometricky tvar, potrebuju konkretnu znacku a aj suhlasny pocet
     * spracovanych udajov. Ak sa ani jedno nezhoduje program vypise chybu
     */
    private void addGeomTvar(double[] pole) {
        if ((pole[0] == 0.0) && (pole.length == 7)) {
            GeomTvar kruh = new Kruh(pole);
            kruh.obsah = kruh.vypocetObsah();
            kruh.obvod = kruh.vypocetObvod();
            kruh.typ = (int) pole[0];
            this.poleGeometrickychTvarov.add(kruh);
        } else if ((pole[0] == 1.0) && (pole.length == 8)) {
            GeomTvar obdlznik = new Obdlznik(pole);
            obdlznik.obsah = obdlznik.vypocetObsah();
            obdlznik.obvod = obdlznik.vypocetObvod();
            obdlznik.typ = (int) pole[0];
            this.poleGeometrickychTvarov.add(obdlznik);
        } else if ((pole[0] == 2.0) && (pole.length == 9)) {
            GeomTvar trojuholnik = new Trojuholnik(pole);
            trojuholnik.obsah = trojuholnik.vypocetObsah();
            trojuholnik.obvod = trojuholnik.vypocetObvod();
            trojuholnik.typ = (int) pole[0];
            this.poleGeometrickychTvarov.add(trojuholnik);
        } else {
            System.out.println("chyba vo velkosti");
        }

    }

    /**
     * @deleteGeomTvar vymaze prislusny geometricky tvar podla indexu, ktory si
     * zvoly uzivatel. Podla vypisu vie uzivatel vzmazat konkretny geometricky
     * tvar. V metode overujem prazdnost ArrayListu, ktory ak je prazdny
     * upozorni uzivatela slovne, ze ziaden objekt uz neexistuje.
     */
    private void deleteGeomTvar() {

        if (!this.poleGeometrickychTvarov.isEmpty()) {
            for (int i = 0; i < poleGeometrickychTvarov.size(); i++) {
                int moznost = poleGeometrickychTvarov.get(i).typ;
                switch (moznost) {
                    case 0:
                        System.out.println("Objekt: " + i);
                        System.out.println(poleGeometrickychTvarov.get(i).toString());
                        break;
                    case 1:
                        System.out.println("Objekt: " + i);
                        System.out.println(poleGeometrickychTvarov.get(i).toString());
                        break;
                    case 2:
                        System.out.println("Objekt: " + i);
                        System.out.println(poleGeometrickychTvarov.get(i).toString());
                        break;
                    default:
                        System.out.println("Ziaden objekt neexistuje");

                }
            }
            int ktory = readInt("\nKtory objekt ma byt vymazany? ");
            if ((ktory < this.poleGeometrickychTvarov.size()) && ktory >= 0) {
                if (this.poleGeometrickychTvarov.get(ktory) != null) {
                    this.poleGeometrickychTvarov.remove(ktory);
                    System.out.println("Objekt vymazany");

                } else {
                    System.out.println("Objekt neexistuje");

                }
            } else {
                System.out.println("Objekt neexistuje");
            }
        } else {
            System.out.println("\nZiaden objekt uz neexistuje\n");

        }

    }

    /**
     * zistuje pocet geometrickych tvarov konkretne aj podla druhu. Nasledne ich
     * vypise
     */
    private void pocetGeomTvar() {
        int pocetKruh = 0;
        int pocetObdlznik = 0;
        int pocetTrojuholnik = 0;
        System.out.println("Celkovy pocet geometrickych objektov: " + poleGeometrickychTvarov.size());
        for (int i = 0; i < poleGeometrickychTvarov.size(); i++) {
            if (poleGeometrickychTvarov.get(i).typ == 0) {
                pocetKruh++;
            } else if (poleGeometrickychTvarov.get(i).typ == 1) {
                pocetObdlznik++;
            } else if (poleGeometrickychTvarov.get(i).typ == 2) {
                pocetTrojuholnik++;
            }
        }
        System.out.println("pocet trojuholnikov: " + pocetTrojuholnik);
        System.out.println("pocet kruhov: " + pocetKruh);
        System.out.println("pocet obdlznikov: " + pocetObdlznik);

    }

    /**
     *
     * @param vstup je nazov vstupneho suboru Ma na vstupe String ktory
     * reprezentuje nazov suboru. Dalej sa stara o prechod z udajov zo vstupneho
     * suboru do double polePomocne. Nasledne z neho extrahujem udaje ktore
     * vzuzivam pry vztvrani jednotlivzch geometrickych utvarov
     */
    private void zoSuboruDoArrayList(String vstup) {
        String[] poleString = nacitajSubor(vstup);
        double[][] polePomocne = poleStringToDouble(poleString);
        for (int i = 0; i < polePomocne.length; i++) {
            addGeomTvar(polePomocne[i]);
        }

    }

    /**
     *
     * @param utvar je pole stringov ktor0ho obsah prekonvertujem na maticu typu
     * double
     * @return matica typu double
     */
    private static double[][] poleStringToDouble(String[] utvar) {

        double matica[][] = new double[utvar.length][];
        String[][] matica2 = new String[utvar.length][];
        for (int i = 0; i < utvar.length; i++) {
            matica2[i] = utvar[i].split(";");
            matica[i] = pomocnePoleStringToDouble(matica2[i]);
        }

        return matica;

    }

    /**
     *
     * @param matica je vstup ktora je jednym riadkom z povodnej matice v metode
     * poleStringToDouble a reprezentuje pole stringov z ktoreho nasledne
     * konvertujeme na double
     * @return vracia double pole ktore reprezentuje udaje o jednom geometrickom
     * tvare.
     */
    /*pomocnePoleStringToInt riesi samotny prevod String do int a kedze sa jedna o dvojrozmerne pole 
      riesim kazdy riadok matice Stringov zvlast.*/
    private static double[] pomocnePoleStringToDouble(String[] matica) {
        //-1 osetrenie toho, ze posledny prvok pola je " "
        double pole[] = new double[(matica.length)];
        for (int i = 0; i < (matica.length); i++) {

            pole[i] = Double.parseDouble(matica[i]);
        }
        return pole;
    }

    /**
     *
     * @param vstup je nazov vstupneho suboru
     * @return vracia pocet riadkov v subore
     */
    private static int pocetRidakov(String vstup) {
        int pocitadlo = 0;
        try {
            FileInputStream subor = new FileInputStream(vstup);
            InputStreamReader kabel = new InputStreamReader(subor);

            BufferedReader zo_suboru = new BufferedReader(kabel);

            while (zo_suboru.readLine() != null) {
                pocitadlo++;
            }
            zo_suboru.close();
            return pocitadlo;

        } catch (IOException e) {
            System.out.println("chyba");
        }
        return pocitadlo;
    }

    /**
     *
     * @param vstup je nazov vstupneho suboru
     * @return vracia pole Stringov z ktorej kazdy prvom reprezentuje jeden
     * geometricky objekt
     */
    private static String[] nacitajSubor(String vstup) {
        try {

            int velkost = 0;
            velkost = pocetRidakov(vstup);
            FileInputStream subor = new FileInputStream(vstup);
            InputStreamReader kabel = new InputStreamReader(subor);
            BufferedReader zo_suboru = new BufferedReader(kabel);
            String[] poleString = new String[velkost];
            int i = 0;
            while (i < velkost) {
                poleString[i] = zo_suboru.readLine();
                i++;
            }

            zo_suboru.close();
            return poleString;
        } catch (IOException e) {
            System.out.println("chyba");
        }
        return null;
    }

    /**
     *
     * @param napis_pre_uzivatela string odkaz pre uzivatela co ma zadat
     * @return vrati integer metoda prevzata od prednasajuceho
     */
    private static int readInt(String napis_pre_uzivatela) {
        int n = 0;
        String s;

        BufferedReader zklavesnice = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.println(napis_pre_uzivatela);
            s = zklavesnice.readLine();
            //System.out.println("Nacital som " + s);
            n = Integer.parseInt(s);
        } catch (Exception e) {
            System.out.println("nepodarilo sa");
            n = readInt(napis_pre_uzivatela);
        }

        return n;
    }

    /**
     * menu sluzi na interakciu medzi pouzivatelom a programom. Vypisuje
     * jednotlive udaje o objektoch
     */
    private void menu() {
        while (true) {
            System.out.println("\nPre vypocet celkoveho obsahu a obvodu zadaj '0' ");
            System.out.println("Pre zistenie poctu Geometrickych tvarov zadaj '1' ");
            System.out.println("Pre vymazanie objektu zadaj '2' ");
            System.out.println("Pre ukoncenie programu zadaj '3' a viac \n");
            int klavesnica = readInt("Vasa volba:");
            switch (klavesnica) {
                case 0:
                    this.sumGeomTvar();
                    break;
                case 1:
                    this.pocetGeomTvar();
                    break;
                case 2:
                    this.pocetGeomTvar();
                    this.deleteGeomTvar();
                    break;
                default:
                    System.out.println("\nProgram skoncil\n");
                    return;

            }
        }

    }

    /**
     *
     * @param e ak sme klikli na objekt ktory posuvame a pustime klik tak
     * nastavujeme v tejto metode zmen objekt = 0 aby sme nepohzbovali objektom
     * aj po pusteni kliku
     */
    @Override
    public void mouseReleased(MouseEvent e) {
        zmenobjekt = null;
    }

    /**
     *
     * @param e Reaguje na kliknutie odstranuje a pridava geometricke tvary
     * podla toho ci je zvoleny choice pridaj alebo odstran. Pocitanie
     * vzdialenosi ci sme alebo niesme v danom utvare respektive ku ktoremu
     * utvaru sme najblizsie.
     */
    @Override
    public void mouseClicked(MouseEvent e) {
        if (indexPridaOdstran == 0) { //ak je to nula maz objekty
            int cx = e.getX();
            int cy = e.getY();

            for (GeomTvar it : this.getPoleGeometrickychTvarov()) {

                a = it.surX - cx;
                b = it.surY - cy;
                double vzdialenost = 100000;
                double stranaA;
                double stranaB;
                switch (it.typ) {
                    case 0:
                        if ((Math.pow((it.surX + ((Kruh) it).getPolomer()) - cx, 2) + Math.pow((it.surY + ((Kruh) it).getPolomer()) - cy, 2)) < Math.pow(((Kruh) it).getPolomer(), 2)) {        //vyberie sa objekt ak je kliknute do kruhu 
                            stranaA = Math.pow((it.surX - cx), 2);
                            stranaB = Math.pow((it.surY - cy), 2);
                            if (Math.sqrt(stranaA + stranaB) < vzdialenost) {
                                this.indexObjekt = poleGeometrickychTvarov.indexOf(it);
                                vzdialenost = Math.sqrt(stranaA + stranaB);

                            }
                        }
                        break;

                    case 1:
                        stranaA = Math.pow((it.surX - cx), 2);
                        stranaB = Math.pow((it.surY - cy), 2);
                        if (cx <= it.surX + ((Obdlznik) it).getSirka() && cx >= it.surX && cy <= it.surY + ((Obdlznik) it).getVyska() && cy >= it.surY) {        //vyberie sa obdlznik ak je kliknute do neho
                            if (Math.sqrt(stranaA + stranaB) < vzdialenost) {
                                this.indexObjekt = poleGeometrickychTvarov.indexOf(it);
                                vzdialenost = Math.sqrt(stranaA + stranaB);
                            }
                        }
                        break;
                    case 2:

                        int vyska = (int) (Math.cos(Math.toRadians(90) - Math.toRadians(((Trojuholnik) it).getUholY())) * ((Trojuholnik) it).getB());
                        int x_a = (int) (Math.sin(Math.toRadians(90) - Math.toRadians(((Trojuholnik) it).getUholY())) * ((Trojuholnik) it).getB());
                        double x1 = it.surX;
                        double y1 = it.surY;
                        double x2 = (it.surX + (int) ((Trojuholnik) it).getA());
                        double y2 = it.surY;
                        double x3 = (it.surX + x_a);
                        double y3 = (it.surY + vyska);

                        double y23 = y2 - y3;
                        double x32 = x3 - x2;
                        double y31 = y3 - y1;
                        double x13 = x1 - x3;
                        double det = y23 * x13 - x32 * y31;
                        double minD = Math.min(det, 0);
                        double maxD = Math.max(det, 0);
                        int pravda = 1;

                        double dx = cx - x3;
                        double dy = cy - y3;
                        double a = y23 * dx + x32 * dy;
                        if (a < minD || a > maxD) {
                            pravda = 0;
                        }
                        double b = y31 * dx + x13 * dy;
                        if (b < minD || b > maxD) {
                            pravda = 0;

                        }
                        double c = det - a - b;
                        if (c < minD || c > maxD) {
                            pravda = 0;
                        }
                        if (pravda == 1) {
                            stranaA = Math.pow((it.surX - cx), 2);
                            stranaB = Math.pow((it.surY - cy), 2);
                            if (Math.sqrt(stranaA + stranaB) < vzdialenost) {

                                this.indexObjekt = poleGeometrickychTvarov.indexOf(it);
                                vzdialenost = Math.sqrt(stranaA + stranaB);

                            }
                        }

                        break;
                }

            }
            if (indexObjekt > (-1)) {
                poleGeometrickychTvarov.remove(indexObjekt);
                this.indexObjekt = (-1);
                repaint();
                return;

            }
        } else if (indexPridaOdstran == 1) { //ak je to jednotka pridavaj objekty 
            switch (index) {
                case 0:
                    int polomer = 20;
                    double[] a = {0, (e.getX() - polomer), (e.getY() - polomer), this.farR, this.farG, this.farB, polomer};
                    addGeomTvar(a);
                    repaint();
                    break;
                case 1:
                    int sirka = 50;
                    int vyska = 20;
                    double[] b = {1, (e.getX() - sirka / 2), (e.getY() - vyska / 2), this.farR, this.farG, this.farB, sirka, vyska};
                    addGeomTvar(b);
                    repaint();
                    break;
                case 2:
                    int stranaA = 30;
                    int stranaB = 30;
                    int uhol = 60;
                    double[] c = {2, (e.getX() - stranaA / 2), (e.getY() - stranaA / 4), this.farR, this.farG, this.farB, stranaA, stranaB, uhol};
                    addGeomTvar(c);
                    repaint();
                    break;
                default:
                    int polomer2 = 40;
                    double[] d = {0, (e.getX() - polomer2), (e.getY() - polomer2), this.farR, this.farG, this.farB, polomer2};
                    addGeomTvar(d);
                    repaint();
                    break;
            }

        }

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void mouseMoved(MouseEvent e) {

    }

    /**
     *
     * @param e Zistuje ci je sme klikli mysou a ci je klik stale drzany
     * nasledne vyhodnocuje to ako sa pohybujeme mysou a podla toho posuva
     * jednotlive geometricke utvary. Ak sme klikli na utvar presuvameho pomocou
     * mysky zaroven zistuje ku ktoremu utvaru sme suradnicovo blizsie.
     */
    @Override
    public void mousePressed(MouseEvent e) {
        int cx = e.getX();
        int cy = e.getY();

        for (ListIterator iterator = poleGeometrickychTvarov.listIterator(poleGeometrickychTvarov.size()); iterator.hasPrevious();) {   //prechadzam list od zadu aby som sa dostal k vrchnemu objektu
            GeomTvar o = (GeomTvar) iterator.previous();
            a = o.surX - cx;
            b = o.surY - cy;
            //roztriedenie podla typov ak vyhovuje typ meni sa poloha utvaru
            if (o.typ == 0) {
                if (Math.sqrt(Math.pow(cx - o.surX - ((Kruh) o).getPolomer(), 2) + Math.pow(cy - o.surY - ((Kruh) o).getPolomer(), 2)) <= ((Kruh) o).getPolomer()) {        //vyberie sa objekt ak je kliknute do kruhu 
                    zmenobjekt = o;
                    updatePosition(e);
                    return;
                }
            } else if (o.typ == 1) {
                if (cx <= o.surX + ((Obdlznik) o).getSirka() && cx >= o.surX && cy <= o.surY + ((Obdlznik) o).getVyska() && cy >= o.surY) {        //vyberie sa obdlznik ak je kliknute do neho
                    zmenobjekt = o;
                    updatePosition(e);
                    return;
                }

            } else if (o.typ == 2) {

                int vyska = (int) (Math.cos(Math.toRadians(90) - Math.toRadians(((Trojuholnik) o).getUholY())) * ((Trojuholnik) o).getB());
                int x_a = (int) (Math.sin(Math.toRadians(90) - Math.toRadians(((Trojuholnik) o).getUholY())) * ((Trojuholnik) o).getB());
                double x1 = o.surX;
                double y1 = o.surY;
                double x2 = (o.surX + (int) ((Trojuholnik) o).getA());
                double y2 = o.surY;
                double x3 = (o.surX + x_a);
                double y3 = (o.surY + vyska);

                double y23 = y2 - y3;
                double x32 = x3 - x2;
                double y31 = y3 - y1;
                double x13 = x1 - x3;
                double det = y23 * x13 - x32 * y31;
                double minD = Math.min(det, 0);
                double maxD = Math.max(det, 0);
                int pravda = 1;

                double dx = cx - x3;
                double dy = cy - y3;
                double a = y23 * dx + x32 * dy;
                if (a < minD || a > maxD) {
                    pravda = 0;
                }
                double b = y31 * dx + x13 * dy;
                if (b < minD || b > maxD) {
                    pravda = 0;

                }
                double c = det - a - b;
                if (c < minD || c > maxD) {
                    pravda = 0;
                }
                if (pravda == 1) {
                    zmenobjekt = o;
                    updatePosition(e);
                    return;
                }

            }
        }
    }

    /**
     *
     * @param e meni poziciu objektu
     */
    public void updatePosition(MouseEvent e) {
        try{
        if (zmenobjekt.typ == 0) {
            zmenobjekt.setSurX(e.getX() + (int) a);
            zmenobjekt.setSurY(e.getY() + (int) b);
        } else if (zmenobjekt.typ == 1) {
            zmenobjekt.setSurX(e.getX() + (int) a);
            zmenobjekt.setSurY(e.getY() + (int) b);
        } else if (zmenobjekt.typ == 2) {
            zmenobjekt.setSurX(e.getX() + (int) a);
            zmenobjekt.setSurY(e.getY() + (int) b);
        }
        this.repaint();
        }catch(NullPointerException a ){
            System.out.println(a);
        }
    }

    /**
     *
     * @param e v tejto metode volame metodu na aktualizaciu posunu mysi po
     * canvase prave vtedy ak potiahneme zaklikutou mysou
     */
    @Override
    public void mouseDragged(MouseEvent e) {
        updatePosition(e);
    }

    /**
     *
     * @param a integer ktorym nastavujeme to ci bude pridany kruh trojuholnik
     * alebo stvorec
     */
    public void setIndex(int a) {
        this.index = a;
    }
}
