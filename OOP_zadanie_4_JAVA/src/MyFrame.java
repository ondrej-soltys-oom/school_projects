
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * @author Ondrej Soltys,
 * ID 80108
 * Zadanie Zadanie c.4
 */
public class MyFrame extends Frame implements ActionListener {

    private Platno plat;
    private ArrayList<GeomTvar> poleGeometrickychTvarov = new ArrayList();
    private Frame mainFrame;
    private Panel southPanel;
    private Button kruh;
    private Button obdlznik;
    private Button trojuholnik;
    private CheckboxGroup cbg;
    private Checkbox pridaj;
    private Checkbox odstran;
    private Choice choice;
    private Label label1;
    private MenuItem ExitItem;
    private MenuItem OpenFile;
    private MenuItem SaveAsFile;

    /**
     *
     * @param utvary vsupny objekt ktory potrebujeme na to aby nam frame
     * vykreslil utvary do cavasu implemetacia celefo MyFrame
     */
    public MyFrame(Platno utvary) {
        plat = utvary;
        createMyFrame(utvary);
        createSouthPanel();
        mainFrame.add("South", southPanel);
        createMenuBar();

        mainFrame.setVisible(true);

    }

    /**
     * vytvorenie MenuBaru za pomoci ktoreho budeme ukladat a otvarat subory
     */
    private void createMenuBar() {
        MenuBar bar = new MenuBar();
        setMenuBar(bar);

        Menu file = new Menu("File");

        OpenFile = new MenuItem("Open");
        OpenFile.addActionListener(this);
        file.add(OpenFile);

        SaveAsFile = new MenuItem("Save as");
        SaveAsFile.addActionListener(this);
        file.add(SaveAsFile);

        ExitItem = new MenuItem("Exit");
        ExitItem.addActionListener(this);
        file.add(ExitItem);

        bar.add(file);
        mainFrame.setMenuBar(bar);
    }

    /**
     *
     * @param utvary vsupny objekt ktory potrebujeme na to aby nam frame
     * vykreslil utvary do cavasu vytvorenie samotneho Frame setovanie Lazoutu
     * pridanie label na sever Frame a pridanie do centra canvas na vykreslenie
     * vytvorenie Listenera na ukoncenie proramu za pomoci x
     */
    private void createMyFrame(Platno utvary) {
        mainFrame = new Frame("Geometricke tvary");
        mainFrame.setSize(700, 700);
        mainFrame.setLayout(new BorderLayout());
        label1 = new Label("Pridavate kruh a mozete posuvat geometricke utvary");
        mainFrame.add("North", label1);
        mainFrame.add("Center", utvary);
        mainFrame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent windowEvent) {
                System.exit(0);
            }
        });
    }

    /**
     * Vyvorenie juzneho panelu kde si vzberame s pomedyi toho, aky utvar
     * pridavame jeho farbu a alebo ci mazeme subor. Vytvorenie listenerov ktore
     * menia text v label + menia to ci budeme pridavat utvary alebo ich mazat.
     * Vytvorenie listenerov na zmenu farby utvarov ktore chceme pridavat.
     */
    private void createSouthPanel() {
        southPanel = new Panel();
        southPanel.setLayout(new GridLayout(1, 6));
        kruh = new Button("Kruh");
        kruh.addActionListener(this);
        obdlznik = new Button("Obdlznik");
        obdlznik.addActionListener(this);
        trojuholnik = new Button("Trojuholnik");
        trojuholnik.addActionListener(this);

        cbg = new CheckboxGroup();
        pridaj = new Checkbox("Pridaj", cbg, true);

        odstran = new Checkbox("Odstran", cbg, false);
        pridaj.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                plat.indexPridaOdstran = 1;
                switch (plat.index) {
                    case 0:
                        label1.setText("Pridavate kruh a mozete posuvat geometricke utvary");
                        break;
                    case 1:
                        label1.setText("Pridavate obdlznik a mozete posuvat geometricke utvary");
                        break;
                    case 2:
                        label1.setText("Pridavate trojuholnik a mozete posuvat geometricke utvary");
                        break;
                }
            }
        });

        odstran.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                plat.indexPridaOdstran = 0;
                label1.setText("Mozte mazat a posuvat geometricke utvary");
            }
        });
        choice = new Choice();
        choice.add("Modra");
        choice.add("Zelena");
        choice.add("Cervena");
        southPanel.add(kruh);
        southPanel.add(obdlznik);
        southPanel.add(trojuholnik);
        southPanel.add(choice);
        southPanel.add(pridaj);

        southPanel.add(odstran);
        choice.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                switch (choice.getItem(choice.getSelectedIndex())) {
                    case "Modra":
                        plat.farB = 255;
                        plat.farG = 0;
                        plat.farR = 0;
                        break;
                    case "Zelena":
                        plat.farB = 0;
                        plat.farG = 255;
                        plat.farR = 0;
                        break;
                    case "Cervena":
                        plat.farB = 0;
                        plat.farG = 0;
                        plat.farR = 255;
                        break;
                    default:
                        System.out.println("vyber neznamej farby");
                        break;
                }

            }
        });
    }

    /**
     *
     * @param e Implementacia listenerov na pridavanie kruhu, obdlzniku,
     * trojuholniku, zatvaranie suboru pomocou MenuBar otvaranie a ukladanie
     * suboru pomocou MenuBar.Ukladanie je realizovane do TXT suborov ktore
     * reprezentuju rozlozenie objektov na Canvase a ich jednotlivych zaladnzch
     * udajov. Otvaranie suborov a prekreslenie Canvasu utvarmi definovanymi v
     * txt subore
     *
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand();
        if (odstran.getState() != true) {
            if (command.equals("Kruh")) {
                label1.setText("Pridavate kruh a mozete posuvat geometricke utvary");
                plat.setIndex(0);
            }
            if (command.equals("Obdlznik")) {
                label1.setText("Pridavate obdlznik a mozete posuvat geometricke utvary");
                plat.setIndex(1);
            }
            if (command.equals("Trojuholnik")) {
                label1.setText("Pridavate trojuholnik a mozete posuvat geometricke utvary");
                plat.setIndex(2);
            }
        }
        if (e.getSource() == ExitItem) {
            System.exit(0);
        }
        if (e.getSource() == OpenFile) {
            FileDialog fd = new FileDialog(mainFrame, "Choose a file", FileDialog.LOAD);
            fd.setDirectory("C:\\");
            fd.setFile("*.txt");
            fd.setVisible(true);
            String filename = fd.getFile();

            if (filename == null) {
                System.out.println("You cancelled the choice");
            } else {
                poleGeometrickychTvarov.removeAll(poleGeometrickychTvarov);
                Platno loadPlatno = new Platno(filename);
                this.plat.setPoleGeometrickychTvarov(loadPlatno.getPoleGeometrickychTvarov());
                plat.repaint();

            }

        }
        if (e.getSource() == SaveAsFile) {
            FileDialog fd = new FileDialog(mainFrame, "Choose a file", FileDialog.SAVE);
            fd.setDirectory("C:\\");
            fd.setFile("*.txt");
            fd.setVisible(true);
            String filename = fd.getFile();

            if (filename == null) {
                System.out.println("You cancelled the choice");
            } else {
                try (PrintWriter writer = new PrintWriter(filename, "UTF-8")) {
                    for (GeomTvar p : plat.getPoleGeometrickychTvarov()) {

                        switch (p.typ) {
                            case 0:
                                writer.println(p.typ + ";" + ((Kruh) p).surX + ";" + ((Kruh) p).surY + ";" + p.colR + ";" + p.colG + ";" + p.colB + ";" + ((Kruh) p).getPolomer() + ";");
                                break;

                            case 1:
                                writer.println(p.typ + ";" + p.surX + ";" + p.surY + ";" + p.colR + ";" + p.colG + ";" + p.colB + ";" + ((Obdlznik) p).getSirka() + ";" + ((Obdlznik) p).getVyska() + ";");
                                break;

                            case 2:
                                writer.println(p.typ + ";" + p.surX + ";" + p.surY + ";" + p.colR + ";" + p.colG + ";" + p.colB + ";" + ((Trojuholnik) p).getA() + ";" + ((Trojuholnik) p).getB() + ";" + ((Trojuholnik) p).getUholY() + ";");
                                break;
                        }
                    }
                } catch (IOException error) {
                    System.out.println("ERROR: " + error);
                }
            }

        }

    }
}
