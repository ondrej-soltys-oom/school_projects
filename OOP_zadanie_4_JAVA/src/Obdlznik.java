/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * @author Ondrej Soltys,
 * ID 80108
 * Zadanie Zadanie c.4
 */
public class Obdlznik extends GeomTvar {

    private double sirka;
    private double vyska;

    public Obdlznik(){
    }
    /**
     *
     * @param pole obsahuje udaje o danom geometrickom tvare, ktore sa pri
     * volani konstruktora nastavia v konstruktore pri vytvoreni objektu naplnim
     * hodnotami dany objekt prostrednictvom set metod
     */
    public Obdlznik(double[] pole) {
        this.setSurX((int) pole[1]);
        this.setSurY((int) pole[2]);
        this.setR((int) pole[3]);
        this.setG((int) pole[4]);
        this.setB((int) pole[5]);
        this.setSirka(pole[6]);
        this.setVyska(pole[7]);

    }

    /**
     *
     * @param vys udaj o vyske geometrickeho tvaru
     */

    public final void setVyska(double vys) {
        this.vyska = vys;

    }

    /**
     *
     * @param sir udaj o sirke geometrickeho tvaru
     */
    public final void setSirka(double sir) {
        this.sirka = sir;

    }

    
    @Override
    public String toString() {
        return "Obdlznik{" + "sirka=" + sirka + ", vyska=" + vyska + '}';
    }

    
    @Override
    public double vypocetObvod() {
        return ((this.vyska * 2) + (this.sirka * 2));
    }

    
    @Override
    public double vypocetObsah() {
        return this.vyska * this.sirka;
    }
    /**
     * 
     * @return vracia sirku obdlznika
     */
    public double getSirka() {
        return sirka;
    }
    /**
     * 
     * @return vracia vysku obdlznika
     */
    public double getVyska() {
        return vyska;
    }

}
