/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * @author Ondrej Soltys,
 * ID 80108
 * Zadanie Zadanie c.4
 */
/*Trieda GeomTvar je abstraktna trieda, ktora obsahuje spolocne metody pre kazdy geometricky tvar
  su v nej 3 abstraktne metody a to konkretne toString obsah a obvod, kotrych implementaciu maju kazda trieda
  rozdielnu. setSurY a setSurX abstraktne niesu lebo kazda trieda ich vyuziva v taktjto podobe. */
abstract public class GeomTvar {

    protected int surX;
    protected int surY;
    protected double obvod;
    protected double obsah;
    protected int typ;
    protected int colR;
    protected int colG;
    protected int colB;

    /**
     *
     * @param x setovanie suradnice X
     */
    public void setSurX(int x) {
        this.surX = x;
    }

    /**
     *
     * @param y  setovanie suradnice Y
     */
    public void setSurY(int y) {
        this.surY = y;
    }
    /**
     * 
     * @param r setovanie farebneho odtieňa colR
     */
    public void setR(int r) {
        this.colR = r;
    }
    /**
     * 
     * @param g setovanie farebneho odtieňa colG
     */
    public void setG(int g) {
        this.colG = g;
    }
    /**
     * 
     * @param b setovanie farebneho odtieňa colB
     */
    public void setB(int b) {
        this.colB = b;
    }
    /**
     * 
     * @return vrati strig urciteho geometrickeho objektu nazov a jeho zakladne udaje
     */
    @Override
    abstract public String toString();
    /**
     * 
     * @return vrati obvod daneho geometrickeho tvaru
     */
    abstract public double vypocetObvod();
    /**
     * 
     * @return vrati obsah daneho geometrickeho tvaru
     */
    abstract public double vypocetObsah();

}
