/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * @author Ondrej Soltys,
 * ID 80108
 * Zadanie Zadanie c.4
 */
public class Trojuholnik extends GeomTvar {

    private double a;
    private double b;
    private double c;

    private double uholY;
    public Trojuholnik(){
    }
    /**
     *
     * @param pole obsahuje udaje o danom geometrickom tvare, ktore sa pri
     * volani konstruktora nastavia v konstruktore pri vytvoreni objektu naplnim
     * hodnotami dany objekt prostrednictvom set metod
     */

    public Trojuholnik(double[] pole) {
        this.setSurX((int) pole[1]);
        this.setSurY((int) pole[2]);
        this.setR((int) pole[3]);
        this.setG((int) pole[4]);
        this.setB((int) pole[5]);
        this.setA(pole[6]);
        this.setB(pole[7]);
        this.setUholY(pole[8]);
        

    }

    /**
     *
     * @param A dlzka strany a
     */
    public final void setA(double A) {
        this.a = A;

    }

    /**
     *
     * @param B dlzka strany b
     */
    public final void setB(double B) {
        this.b = B;

    }

    /**
     *
     * @param uhol nastavuje uhol medzi a a b
     */
    public final void setUholY(double uhol) {
        this.uholY = uhol;
    }
    /**
     * 
     * @return vrati dlzku strany a
     */
    public double getA() {
        return this.a;
    }
    /**
     * 
     * @return vrati dlzku strany b
     */
    public double getB() {
        return this.b;
    }
    /**
     * 
     * @return vrati dlzku strany c
     */
    public double getC() {
        return this.c;
    }
    /**
     * 
     * @return vrati velkost uhla Y
     */
    public double getUholY() {
        return this.uholY;
    }

    /**
     *
     * @return returnne string ktorz patri danemu geometrickemu utvaru
     */
    @Override
    public String toString() {
        return "Trojuholnik{" + "a=" + a + ", b=" + b + ", uholY=" + uholY + '}';
    }

    
    @Override
    public double vypocetObvod() {
        double cos = Math.cos(Math.toRadians(this.uholY));
        double abCosY2 = 2 * (this.a * this.b * cos);
        double medziVysledok = Math.pow(this.a, 2) + Math.pow(this.b, 2) - abCosY2;
        this.c = Math.sqrt(medziVysledok);        
        return (this.a + this.b + this.c);
    }

    
    @Override
    public double vypocetObsah() {
        double sin = Math.sin(Math.toRadians(this.uholY));
        double vyskaNaStranuA = (this.b * sin);
        return ((this.a * vyskaNaStranuA) / 2);
    }

}
