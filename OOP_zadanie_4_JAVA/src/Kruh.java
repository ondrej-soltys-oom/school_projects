/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * @author Ondrej Soltys,
 * ID 80108
 * Zadanie Zadanie c.4
 */
public class Kruh extends GeomTvar {

    private double polomer;
    public Kruh() {       
    }
   /**
     * 
     * @param pole obsahuje udaje o danom geometrickom tvare, ktore sa pri volani konstruktora nastavia
     * v konstruktore pri vytvoreni objektu naplnim hodnotami dany objekt prostrednictvom set metod
     */
    public Kruh(double[] pole) {
        this.setSurX((int) pole[1]);
        this.setSurY((int) pole[2]);
        this.setR((int) pole[3]);
        this.setG((int) pole[4]);
        this.setB((int) pole[5]);
        this.setPolomer(pole[6]);

    }
    /**
     * 
     * @param pol udaj o velkosti polomeru kruhu
     */
    public final void setPolomer(double pol) {
        this.polomer = pol;

    }
    
    @Override
    public String toString() {
        return "Kruh{" + "polomer=" + polomer + '}';
    }
    
    @Override
    public double vypocetObvod() {
        return (2 * this.polomer * Math.PI);
    }
    
    @Override
    public double vypocetObsah() {
        return (Math.PI * Math.pow(this.polomer, 2));
    }
    /**
     * 
     * @return vrati polomer kruhu
     */
    public double getPolomer() {
        return polomer;
    }

}
