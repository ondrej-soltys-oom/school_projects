
#include "EBeeHives.h"

EBeeHives a;
int interrupt = 0;
const int buttonPin10= 10;
const int buttonPin11= 11;
 int buttonState10 = 0;
 int buttonState11 = 0;
 int lastButtonState = 0;
 
void setup() {
  
   pinMode(buttonPin10, INPUT);
   pinMode(buttonPin11, INPUT);
  a  = EBeeHives();
  a.initialization();
  
}

void loop() {
  
  
  buttonState10 = digitalRead(buttonPin10);
  buttonState11 = digitalRead(buttonPin11);
    //zistuje, ze ci bolo stlacene tlacidlo na pine 10
  if(buttonState10 !=1){
	  //kalibracia vahy po kazdom uvedeni ula do prevadzky
	 a.calibration();
     lastButtonState=0;
     buttonState10=1;
  }

    //zistuje, ze ci bolo stlacene tlacidlo na pine 11
  if(buttonState11 !=1){
      
  lastButtonState=1;
   buttonState11=1;
  
  }
    
   if(lastButtonState == 1){
	   //vykresluje stav ula na displej v zneni Ul je vypnuty
        a.initializationDrawInterrupt();
   }
   else{
	   //skontroluje cas a prejde prvych 10 sms a ak sa tam nachadza ta od vcelara sprostredkuje odosielanie spatnej
		a.acceptSMS();
	   //skontroluje cas a odolse data na server
		a.sendData();
	   //vykresli na dispej	cas a hodnoty vahy, teploty a vlhkosti
        a.initializationDraw();
   }
   //zisti, ci nedochadza k otrasom a ak ano odosle upozornujucu sms
	a.sendAlertSMS();
		
  

}
