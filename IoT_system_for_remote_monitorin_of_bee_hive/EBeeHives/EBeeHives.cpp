


#include "EBeeHives.h"
//GSM
Sim800l Sim800l;
//sht31
Adafruit_SHT31 sht31 = Adafruit_SHT31();
//displej
U8GLIB_SSD1306_128X64 oledDisp(U8G_I2C_OPT_NONE);
//hx711 vaha
HX711 scale(A0, A1);
//otrasi 
MPU9250 mpu = MPU9250();
//struktura pre zakladnu inicializaciu premennych
struct Configuration {
    char url[60];
    char telephone[15];
    char smsCode[20];
    int smsFlag;
    int humidityFlag;
    int temperatureFlag;
    int weightFlag;
    int vibrationFlag;
    int perDayFrequency;
    float humidityTrash;
    float temperatureTrash;
    float weightTrash;
    float vibrationTrash;
    int firstSMS;
	int id;
};
Configuration conf;
File myFile;
//premenny pre potreby odosielania dat
char *apn = "internet";                       //APN
char *apn_u = "ppp";                     //APN-Username
char *apn_p = "ppp";                     //APN-Password
char *url = "vcely.ddns.net:3000/test";

int pinCS = 53; 
int day, month, year, minute, second, hour;
//kalibracny faktor ako hodnota viac v dokumentacii o kalibracii
float calibration_factor = -2070; 
//premenne pre potreby vahy
float units;
float ounces;
//premene pre jednotlive povolenia snimania danich senzorov
float weight = 0;
float gValue = 0;
int smsFirst = 0;
//id ulu
//mozne rozsirenie o viac ulov
int id = 1;
//text sms
char text[200];
//cislo a sms code
char *number;
bool error; //odpoved smssend ak by sme chceli odchytavat 
char *smsCode;
// jednotlive flagy ktore rozhoduju, ze ci dany senzor bude snimat. V pripade sms ide o zadazania sms
int smsFlag = 1;
int humidityFlag = 1;
int temperatureFlag = 1;
int weightFlag = 1;
int dataFlag = 1;
int vibrationFlag = 1;
int perDayFrequency = 4;
//upozornujuce sms v pripade poplachu
int smsHumidity = 0, smsTemperature = 0, smsWeight = 0, smsVibration = 0;
//jednotlive tresh holdy
float humidityTrash = 70;
float temperatureTrash = 40;
float weightTrash = 180;
float vibrationTrash = 300;
char *smsNumber;
//alarm pre posun v smere X, Y, Z dana pozicia arduina
float alarmX, alarmY, alarmZ;
float X, Y, Z;
//nutne v pripade casovania acceptSMS a sendData
int timeBefore = 0;
int timeSMSbefore = 0;


EBeeHives::EBeeHives() {


}
// inicializacia kniznic potrebnych pre pouzivanie jednotlivych softverovich rieseni
void EBeeHives::initialization() {

	delay(256);
    Wire.begin();
    Serial.begin(9600);
    Sim800l.begin(); // initializate the library. 
	//odkomentovat v pripade aktivacie MPU9250
	/*
	uint8_t temp = mpu.begin();
    bool value = acceleration();
	*/
	
	//nastavenie si hodnot pre alarm otrasu
    alarmX = X;
    alarmY = Y;
    alarmZ = Z;
    while (!Serial);
	//vypis pre pripad debagu
    //Serial.println("Instalacia");
	pinMode(pinCS, OUTPUT);
    //inicializacia kniznice pre senzor sht31	 
    if (!sht31.begin(0x44)) {   
        
        while (1) delay(1);
    }
	//inicializacia sd karty
    if (SD.begin()) {
        Serial.println("SD card is ready to use.");

    } else {
        Serial.println("SD card initialization failed");
        return;
    }
    //vytahovanie udajov z EEPROM
    EEPROM.get(0, conf);    
    number = conf.telephone;
    url = conf.url;
    smsCode = conf.smsCode;
    humidityTrash = conf.humidityTrash;
    temperatureTrash = conf.temperatureTrash;
    weightTrash = conf.weightTrash;
    vibrationTrash = conf.vibrationTrash;
    smsFlag = conf.smsFlag;
    Serial.println(smsFlag);
    humidityFlag = conf.humidityFlag;
    weightFlag = conf.weightFlag;
    vibrationFlag = conf.vibrationFlag;
    perDayFrequency = conf.perDayFrequency;
	id = conf.id;
    scale.set_scale();         // tato metoda skaluje podla kalibracneho faktora --nutne kvoli spravne inicializacie
    scale.tare();              // reset skalovania na 0 -- uvodna vaha bude 0
	
    Serial.println("Initialization");
}

//metoda ako vytup vrati namerane udaje z vahy
float EBeeHives::getWeight() {
    scale.set_scale(calibration_factor); 
   units = scale.get_units(), 10;
   if(units < 0)
   {
    units = 0.00;
    }
    ounces = units * 0.035274;
	if (weightFlag == 0) {
        return 0.0;
    } else
        return ((units / 10)-weight);
}

//metoda ako vytup vrati namerane udaje teploty
float EBeeHives::getTemperature() {
    float t = sht31.readTemperature();
    if (temperatureFlag == 0) {
        return 0.0;
    } else
        return t;
}

//metoda ako vytup vrati namerane udaje vlhkosti
float EBeeHives::getHumidity() {
    float h = sht31.readHumidity();
    if (humidityFlag == 0) {
        return 0.0;
    } else
        return h;
}

//metoda ulozi do vytvoreneho suboru namerane udaje
void EBeeHives::putIntoMemory() {
	//vytvorenie si JSON objektu
    StaticJsonBuffer<200> jsonBuffer;
    JsonObject &root = jsonBuffer.createObject();
	//aktualizacia RTC v GSM module
    Sim800l.RTCtime(&day, &month, &year, &hour, &minute, &second);
	//udaje ktore budu ulozene
    String dateData = String(year) + "-" + String(month) + "-" + String(day);
    String timeData = String(hour) + ":" + String(minute) + ":" + String(second);
    root["Id"] = String(id);
    root["Weight"] = getWeight();
    root["Humidity"] = getHumidity();
    root["Temperature"] = getTemperature();
    root["Vibration"] = acceleration();
    root["Date"] = dateData;
    root["Time"] = timeData;
    String sdText;
    root.printTo(sdText);
	//vytvorenie alebo otvorenie suboru vo formate yy_mm_dd.txt
    String bufferFileName = (String(year)) + "_" + (String(month)) + "_" + (String(day)) + ".txt";

    char fileName[bufferFileName.length() + 1];

    bufferFileName.toCharArray(fileName, sizeof(fileName));
    myFile = SD.open(fileName, FILE_WRITE);
	//zapisanie do suboru ak sa uspedne otvoril
    if (myFile) {

        myFile.println(sdText);
        myFile.close(); // close the file

    } else {
        Serial.println("error opening ");
    }


}

//odosle sms vcelarovi ak ma od neho spravu v pamati
void EBeeHives::acceptSMS() {
	//aktualizacia GSM RTC
    Sim800l.RTCtime(&day, &month, &year, &hour, &minute, &second);
    //zistenie si nepresiel urcity cas v tomto pripade kazde 4 minuty
	if ((minute % 4) == 0 && timeSMSbefore != minute) {
        timeSMSbefore = minute;
        String textSms, numberSms, justText;

        for (int i = 0; i < 10; i++) {
			//citanie sms 
            textSms = Sim800l.readSms(i + 1);

            if (textSms.indexOf("OK") != -1) {

				//ziskanie cisla sms
                numberSms = Sim800l.getNumberSms(i + 1);  
				//ziskanie textu sms
                justText = Sim800l.getTextSms(i + 1);
				//odoslanie textu sms do funkcie na parsovanie JSON formatu sms, kedze prijimame sms len v tom formate
                parseJson(justText, numberSms);


            }

        }

        Sim800l.delAllSms();
        //sendSMS();
    }
}
//parsovanie textu sms vo fomate JSON
void EBeeHives::parseJson(String json, String numberSms) {
    StaticJsonBuffer<200> jsonBuffer;
    char *smsApp;
    numberSms.toCharArray(smsApp, (numberSms.length() + 1));
    while (!Serial) {
        // wait serial port initialization
    }
	//vytvorenie JSOM objektu
    JsonObject &root = jsonBuffer.parseObject(json);
    
    if (!root.success()) {
        
        return;
    }
    
    String smska = root["sms"];
    /*podmienky o aku sms sa jedna v tomto pripade o inicializacnu preto porovnava vias parametrov ako je 
	  smsFirst -hovory o tom, ze ci bola prijata prvotna sms
	  smska -hovori o tom ze si arduino ma opravnenie prijmat sms
	  smsCode je unikatny kod, ktory ak obsahuje sms tak vieme prijat prvotnu sms a rozparsovat ju
	*/
    if (((smska == "1") && (smsFirst == 0)) || ((smska == "1") && (smsCode == root["adminKey"]))) {
        //deserializacia JSON
        const char *tn = root["tn"];
        const char *urlsms = root["url"];
        const char *adminKeySms = root["adminKey"];
        //inicializacia premennych na zaklade sms
		number = tn;
        smsCode = adminKeySms;
        url = urlsms;
       	conf.firstSMS = 1;
		//kopirovanie do pamate kvoli naslednemu ulozeniu do EEPROM
        memcpy(conf.url, url, 60);
        memcpy(conf.telephone, number, 15);
        memcpy(conf.smsCode, smsCode, 20);
        EEPROM.put(0, conf);
        sendSMS();

    }
	//vypne alebo zapne notifikaciu sms
    if (smska == "2" && (String) number == numberSms) {
        String smsStop = root["nt"];
        smsFlag = smsStop.toInt();
        sendSMS();
    }
	//odosle aktualne data s konkretneho ula
	//priprava pre mozne odosielanie pre konkretne ule
    if (smska == "3" && (String) number == numberSms) {
        String poleUl = root["hid"];
        //Riesenie len pre jeden ul
        sendSMS();
        
    }
	//nastavenia trash holdov pomocou sms a ulozenie hodnot do premennych ako aj do EEPROM
    if (smska == "4" && (String) number == numberSms) {
        String poleUl = root["hid"];
        String temperature = root["tmp"][0];
        String humidity = root["hmd"][0];
        String weight = root["wght"][0];
        String vibration = root["vib"][0];
        String temperature2 = root["tmp"][1];
        String humidity2 = root["hmd"][1];
        String weight2 = root["wght"][1];
        String vibration2 = root["vib"][1];
        temperatureFlag = temperature.toInt();
        humidityFlag = humidity.toInt();
        weightFlag = weight.toInt();
        vibrationFlag = vibration.toInt();
        temperatureTrash = temperature2.toFloat();
        humidityTrash = humidity2.toFloat();
        weightTrash = weight2.toFloat();
        vibrationTrash = vibration2.toFloat();
        conf.humidityTrash = humidityTrash;
        conf.temperatureTrash = temperatureTrash;
        conf.weightTrash = weightTrash;
        conf.vibrationTrash = vibrationTrash;
        conf.smsFlag = smsFlag;
        conf.humidityFlag = humidityFlag;
        conf.weightFlag = weightFlag;
        conf.vibrationFlag = vibrationFlag;
        EEPROM.put(0, conf);
        sendSMS();
    }
	//zmena frekvencie odosielania dat na server
    if (smska == "5" && (String) number == numberSms) {
        String perDay = root["per"];
        perDayFrequency = perDay.toInt();
        conf.perDayFrequency = perDayFrequency;
        EEPROM.put(0, conf);
        sendSMS();
    }


}
//odosle sms s nameranimi hodnotami 
// pouzitie kniznice SIM800l
void EBeeHives::sendSMS() {
    putIntoMemory();
    if (smsFlag == 1) {
        StaticJsonBuffer<200> jsonBuffer;
        JsonObject &root = jsonBuffer.createObject();
        root["wght"] = getWeight();
        root["hmd"] = getHumidity();
        root["tmp"] = getTemperature();
        root["vib"] = acceleration();
		//transformacia stringu na pole charov 
        String text2;
        root.printTo(text2);
        text2.toCharArray(text, (text2.length() + 1));
        error = Sim800l.sendSms(number, text);
    }
}

void EBeeHives::sendAlertSMS() {

    //int smsHumidity=0, smsTemperature=0, smsWeight=0;
    //odkomentovat v pripade aktivacie MPU9250
	/*
	if (smsFlag == 1 && acceleration() == true && smsVibration == 0) {
        char *smsText = "Prekrocena hodnota vibracii.";
        error = Sim800l.sendSms(number, smsText);
        smsVibration = 1;
        putIntoMemory();
        sendData();
    }
	*/
    if (smsFlag == 1 && getHumidity() >= humidityTrash && smsHumidity == 0) {
        char *smsText = "Prekrocena hodnota vlhkosti.";
        error = Sim800l.sendSms(number, smsText);
        smsHumidity = 1;
        putIntoMemory();
        sendData();
    }
    if (smsFlag == 1 && getTemperature() >= temperatureTrash && smsTemperature == 0) {
        char *smsText = "Prekrocena hodnota teploty.";
        error = Sim800l.sendSms(number, smsText);
        smsTemperature = 1;
        putIntoMemory();
        sendData();
    }
    if (smsFlag == 1 && getWeight() >= weightTrash && smsWeight == 0) {
        char *smsText = "Prekrocena hodnota vahy.";
        error = Sim800l.sendSms(number, smsText);
        smsWeight = 1;
        putIntoMemory();
        sendData();
    }


}

//porovna cas a odosle data na server v pozadovanom formate JSON
void EBeeHives::sendData() {
    Sim800l.RTCtime(&day, &month, &year, &hour, &minute, &second);
	//porovnanie, ze ci nastal cas a je povolene odosielat udaje
    //if (((hour % perDayFrequency) == 0) && dataFlag == 1 && (hour != timeBefore)) {
    if (((hour % perDayFrequency) == 0) && dataFlag == 1 && (hour != timeBefore)) {
	    timeBefore = hour;
		//vlozenie udajov aj do pamate sd 
        putIntoMemory();
        smsVibration = 0;
        smsHumidity = 0;
        smsTemperature = 0;
        StaticJsonBuffer<200> jsonBuffer;
        JsonObject &root = jsonBuffer.createObject();
        String dateData = String(year) + "-" + String(month) + "-" + String(day);
        String timeData = String(hour) + ":" + String(minute) + ":" + String(second);
        root["Id"] = String(id);
        root["Weight"] = getWeight();
        root["Humidity"] = getHumidity();
        root["Temperature"] = getTemperature();
        if (acceleration()) {
            root["Vibration"] = 1;
        } else {
            root["Vibration"] = 0;
        }
        root["Date"] = dateData;
        root["Time"] = timeData;
		//transformacia stringu na pole charov 
        String text2;
        root.printTo(text2);
        text2.toCharArray(text, (text2.length() + 1));
		//odosielanie dat na server apn_u a apn_p su nepovinne ak netreba overovanie
		//mozne rozsirenie o overovanie pri odosielani udajov na server
		Sim800l.sendData(apn, apn_u, apn_p, url, text);

    }

}

//vykreslovanie pri standardnej cinnosti arduina
void EBeeHives::initializationDraw() {

    
    Sim800l.RTCtime(&day, &month, &year, &hour, &minute, &second);
    oledDisp.firstPage();
    do {
        
        draw();
    } while (oledDisp.nextPage());

}

//vykreslovanie udajov ako cas, vaha, teplota a vlhkost
void EBeeHives::draw() {


    float t = getTemperature();
    float h = getHumidity();
	float w = getWeight();
    // nastavenie písma, toto pismo upoznuje vypisat
    // priblizne 15x4 znakov
    oledDisp.setFont(u8g_font_unifont);
	//vykreslenie casu
    oledDisp.setPrintPos(0, 10);
    oledDisp.print("Time");
    oledDisp.print(" ");    
    oledDisp.print(hour, DEC);    
    oledDisp.print(":");
    if (minute < 10) {
        oledDisp.print("0");
    }
    oledDisp.print(minute, DEC);
    oledDisp.print(":");
    oledDisp.print(second, DEC);

    oledDisp.setPrintPos(0, 25);
    //vykreslenie hmotnosti
	if (!isnan(t)) {  
        oledDisp.print("Wght: ");
        oledDisp.print(w);
    } else {
        oledDisp.println("errorTep");
    }
    oledDisp.setPrintPos(0, 40);
	//vykreslenie teploty
    if (!isnan(t)) {  
        oledDisp.print("Tem: ");
        oledDisp.print(t);
    } else {
        oledDisp.println("errorTep");
    }
    oledDisp.setPrintPos(0, 55);
	//vykreslenie vlhkosti
    if (!isnan(h)) {  // check if 'is not a number'
        oledDisp.print("Hum: ");
        oledDisp.print(h);
    } else {
        oledDisp.println("errorVL");
    }


}

//vykreslovanie v rezime prerusenia
void EBeeHives::initializationDrawInterrupt() {
    oledDisp.firstPage();
    do {
        
        drawInterrupt();
    } while (oledDisp.nextPage());
}

//vykreslenie informacie ul je vypnuty
void EBeeHives::drawInterrupt() {


    oledDisp.setFont(u8g_font_unifont);
    oledDisp.setPrintPos(0, 10);
    oledDisp.print("Ul je vypnuty");


}
//kalibracia vahy
void EBeeHives::calibration() {
    smsWeight = 1;
	//rovnaka inicializacia ako pri zapnuti zariadenia
    scale.set_scale();                      
    scale.tare();   
    float weightSum = 0;
	//priemerna hodnota kvoli alermu pre moznost vyzbierat med
    for (int i = 0; i < 10; i++) {
        units = scale.get_units(), 10;
        if (units < 0) {
            units = 0.00;
        }
        weightSum += (units * 0.035274);
    }
    weight = ((weightSum / 10) / 10);

    putIntoMemory();

}
// meranie akceleracie
bool EBeeHives::acceleration() {
    if (vibrationFlag == 1) {
        float sumX, sumY, sumZ;
        for (int i = 0; i < 10; i++) {
			//meranie posunu v x, y, z osi
            mpu.get_accel();
            sumX += abs(mpu.x);
            sumY += abs(mpu.y);
            sumZ += abs(mpu.z);
            delay(100);
        }
        X = sumX / 10;
        Y = sumY / 10;
        Z = sumZ / 10;
		//vyhodnocovanie akceleracie ak vyhodnoti vrati true
        if ((alarmX + vibrationTrash) < X || (alarmY + vibrationTrash) < Y || (alarmZ + vibrationTrash) < Z) {
            return true;
        }
        return false;
    } else
        return false;


}








