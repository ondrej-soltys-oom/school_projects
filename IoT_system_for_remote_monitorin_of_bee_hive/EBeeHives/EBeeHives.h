
#ifndef EBeeHives_h
#define EBeeHives_h

#include <Sim800l.h>
#include <Arduino.h>
#include <Wire.h>
#include "Adafruit_SHT31.h"
#include "HX711.h"
#include "U8glib.h"
#include <SD.h>
#include <SPI.h>
#include <ArduinoJson.h>
#include "MPU9250.h"
#include <SoftwareSerial.h>
#include <EEPROM.h>
#ifndef DS3231_I2C_ADDRESS
#define DS3231_I2C_ADDRESS 0x68

#endif

#ifndef GSM_RX
#define GSM_RX 9
#endif
#ifndef GSM_TX
#define GSM_TX 12
#endif
#ifndef RESET
#define RESET 2
#endif


class EBeeHives{
  


  
  
public: 

  
 EBeeHives();
 
 void initialization();
  
 float getWeight(); 
 
 float getTemperature();
 
 float getHumidity();
 
 void putIntoMemory();
 
 void acceptSMS();
 
 void parseJson(String json,  String numberSms);
 
 void sendData();
 
 void sendSMS();
 
 void sendAlertSMS();
 
 void initializationDraw();
 
 void draw();
 
 void initializationDrawInterrupt();
 
 void drawInterrupt();
  
 void calibration();
 
 bool acceleration();
 
 
  
};



#endif
