var jData;
var slideindex;
var permission = false; // permisson = true ak stlacime play
var pocetSpusteni = 0; // aby sme osetrili viacnasobny autoPlay()

function readTextFile(file, callback) {
    var rawFile = new XMLHttpRequest();
    rawFile.overrideMimeType("application/json");
    rawFile.open("GET", file, true);
    rawFile.onreadystatechange = function() {
        if (rawFile.readyState === 4 && rawFile.status == "200") {
            callback(rawFile.responseText);
        }
    }
    rawFile.send(null);
}

//usage:
readTextFile("tsconfig.json", function(images){
    jData = JSON.parse(images);
    console.log(jData);
});




function box(c) {
    var lb = document.getElementById('lightBox');
    var lb2 = document.getElementById('inner');

    if(lb.style.display != 'block' && lb2.style.display != 'block'){
        lb.style.display = 'block';
        lb2.style.display = 'block';
    }

    var pic = document.getElementById("obr");
    slideindex = c;
    document.getElementById('semka').innerHTML  = '<img class="foto" src="'+jData.images[slideindex-1].url+'" alt="Smiley face">';

    document.getElementById('title').innerHTML = jData.images[slideindex-1].caption;
    document.getElementById('description').innerHTML = jData.images[slideindex-1].description;
}

function prev() {
    if(slideindex == 1)
        slideindex = jData.images.length + 1;
    box(slideindex - 1);
}

function next() {
    if(slideindex == jData.images.length)
        slideindex = 0;
    box(slideindex + 1);
}

function autoPlay(c) {
    pocetSpusteni += c;
    if(!permission || pocetSpusteni > 1){
        permission = false;
        if(pocetSpusteni > 1)
            slideindex--;
        pocetSpusteni = 0;
        return ;
    }

    if(slideindex == jData.images.length + 1)
        slideindex = 1;

    box(slideindex);
    slideindex++;
    setTimeout("autoPlay(0)", 1500);
}

function stop() {
    permission = false;
    slideindex = 1;
    pocetSpusteni = 0;
}

function enable() {
    permission = true;
}

function closeBox() {
    var lb = document.getElementById('inner');
    var lb2 = document.getElementById('lightBox');
    lb.style.display = 'none';
    lb2.style.display = 'none';
}



