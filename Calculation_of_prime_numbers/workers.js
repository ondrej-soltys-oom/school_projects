(function(s) {

    function findPrimeNumbers(hornyInterval, dolnyInterval) {

        if ((Number(dolnyInterval) < 0) || (Number(hornyInterval) < 0)
            || !(/[0-9]/.test(dolnyInterval)) || !(/[0-9]/.test(hornyInterval))) {
            hornyInterval = 0;
            dolnyInterval = 1;
        }

        var isPrime = false;
        var p = 0;
        var primeList = [];
        var list = [];
        var previousProgress;

        for (var i = Number(dolnyInterval); i <= Number(hornyInterval); i++) {
            if (i > 1)
                list.push(i);
        }
        var max = Math.ceil(Math.sqrt(hornyInterval));

        for(var i = 0; i < list.length; i++) {
            isPrime = true;
            /*primeList.splice(0, primeList.length); */

            for (var j = 2; j <= max; j++) {
                if ((list[i] != j) && (list[i] % j == 0)) {
                    isPrime = false;
                } else if ((j == max) && isPrime) {
                    primeList.push(list[i]);
                }
            }

            var percentage = Math.round(i/list.length*100);
            if ((percentage != previousProgress)) {
                s.postMessage({
                    messageType: "Progress",
                    prog: percentage
                });
                previousProgress = percentage;
            }
        }

        s.postMessage({
            messageType: "Result",
            primes: primeList
        });
    }

    s.addEventListener('message', function(e) {
        findPrimeNumbers(JSON.parse(e.data).hornyInterval, JSON.parse(e.data).dolnyInterval);
    });

}(self));