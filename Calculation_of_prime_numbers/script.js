(function(w, d) {
    var myWorker;

    d.querySelector('.count').addEventListener('click', function() {

        $('.prvocisla').hide();

        if(typeof(Worker) !== "undefined") {
            if( (typeof(myWorker) == "undefined") || (myWorker == null) ) {
                myWorker = new Worker('workers.js');
            }
        } else {
            alert("Sorry, your browser does not support Web Workers...");
        }

        myWorker.addEventListener('message', function(e) {
            var primes = e.data.primes;

            if(e.data.messageType == "Result"){
                if (e.data.primes.length == 0){
                    $('#status').text("Skús znova!");
                    return ;
                }

                primes = e.data.primes;
                $('#status').text("100% z výpočtu");
                $('.prvocisla').empty().append(primes.join(", ")).fadeIn(1000);


            } else if(e.data.messageType == "Progress") {

                $('#status').text(e.data.prog + "% z výpočtu");

            }
        });

        var interval = {
            'dolnyInterval': d.querySelector('#begin').value,
            'hornyInterval': d.querySelector('#end').value
        };

        myWorker.postMessage(JSON.stringify(interval));

    });

    d.querySelector('.cancel').addEventListener('click', function() {
        myWorker.terminate();
        myWorker = null;
        $('#status').text("Zrušené!!!");
        $('.prvocisla').hide();


    });

}(window, document));